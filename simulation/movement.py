from typing import Optional, List, Dict, Any

from model import ZERO, FLT, TWO, MoveScript, AutoCancel, PlayerState, Position, Other, PositionShifts, Force
from simulation.physical_collision import BOUND
from .log import transition_logger, position_logger


def compute_movement(p: PlayerState, script: MoveScript, opponent_pos_x: FLT, opponent_pos_y: FLT) -> Optional[AutoCancel]:
    """
    Compute the actual movements

    :param p: the player
    :param script: the script to execute
    :param opponent_pos_x: the opponent position before the position step
    :return: the possible cancel to transition to
    """
    pos = p.pos
    pos.t = None
    # save the dist between players at the beginning of the step
    dist_x = opponent_pos_x - pos.coord[0]
    # pre-compute others bloc
    time = p.script.time

    # Changes of forces
    reset_temporary = set()
    for t in range(min(p.script.previous_time + 1, time), time + 1):
        for f in script.get_forces(t):
            apply_force_element(f, pos)
            if f.is_temporary() and not f.is_one_frame():
                reset_temporary.add(f.type)

    # gather status changes
    sign = gather_status(p, script, dist_x > 0 if dist_x != 0 else pos.side)

    # fetch position changes from script
    if not p.disable_positions:
        pos.shift[0] = sign * script.position_shifts.get(PositionShifts.X, time, ZERO)
        y_shift = script.position_shifts.get(PositionShifts.Y, time, None)
        if y_shift is None:
            # BUGSFV if we just go out of a y_shift we must take the previous state
            y_shift = script.position_shifts.get(PositionShifts.Y, time - 1, ZERO)
        pos.shift[1] = y_shift

    # apply teleport by having a relative x position
    x_relative = script.position_shifts.get(PositionShifts.X | PositionShifts.RELATIVE, time)
    if x_relative is not None:
        pos.ref[0] = opponent_pos_x + sign * x_relative

    # apply forces to the current state
    if p.stun is None:
        apply_forces(p, pos, sign)

    # make sure the coord reflects ref+shift
    pos.coord[0] = pos.ref[0] + pos.shift[0]
    pos.coord[1] = pos.ref[1] + pos.shift[1]

    # compute if we have reached the ground and if so, if we transition from this
    go_to_next = reach_ground_transition(p, pos, script)

    # reset temporary status
    if 1 not in reset_temporary:
        pos.temp_speed[0] = None
    if 2 not in reset_temporary:
        pos.temp_speed[1] = None
    if 4 not in reset_temporary:
        pos.temp_force[0] = None
    if 5 not in reset_temporary:
        pos.temp_force[1] = None

    # compute other move shifts like homing
    others = script.get_others(time)
    if others:
        apply_homing_shifts(pos, others, sign, script.index, dist_x, opponent_pos_x, opponent_pos_y)

    return go_to_next


def apply_force_element(f: Force, pos: Position):
    if f.type == 0:
        return
    if not (f.flag & 0xE) and (f.tick_end - f.tick_start) > 1:
        return

    amount = FLT(f.amount)

    if f.is_on_speed_change():
        pos.on_speed_change[f.type - 1] = amount
        return
    else:
        pos.on_speed_change[f.type - 1] = None

    if f.type == 1:
        # x speed
        if f.is_self_multiply():
            pos.speed[0] *= amount
        elif f.is_temporary():
            pos.temp_speed[0] = amount
        else:
            pos.speed[0] = amount  # * FLT(script.slide or 1)
    elif f.type == 2:
        # y speed
        if f.is_self_multiply():
            pos.speed[1] *= amount
        elif f.is_temporary():
            pos.temp_speed[1] = amount
        else:
            pos.speed[1] = amount  # * (script.slide_y or 1)
    elif f.type == 4:
        # x accel
        if f.is_self_multiply():
            pos.force[0] *= amount
        elif f.is_temporary():
            pos.temp_force[1] = amount
        else:
            pos.force[0] = amount
    elif f.type == 5:
        # y accel
        if f.is_self_multiply():
            pos.force[1] *= amount
        elif f.is_temporary():
            pos.temp_force[1] = amount
        else:
            pos.force[1] = amount


def gather_status(p: PlayerState, script: MoveScript, current_side: bool) -> FLT:
    """
    Update the player statuses using script.states

    :param p: the player state
    :param script: the current script
    :param current_side: the desired side (for the auto-correct computation)
    :return: the sign to use as side in computations
    """
    p.status_air = p.status_counter = False
    p.status_crouched = p.script.script == 1
    sign = None
    for state in script.get_states(p.script.time):
        if state.is_air():
            p.status_air = True
        if state.is_crouched():
            p.status_crouched = True
        if state.is_counter():
            p.status_counter = True
        if state.is_auto_correct():
            p.pos.side = current_side
            sign = FLT(1 if p.pos.side else -1)
        if state.is_reverse_auto_correct():
            p.pos.side = not current_side
            sign = FLT(1 if p.pos.side else -1)
    if sign is None:
        return FLT(1 if p.pos.side else -1)
    return sign


def apply_forces(p: PlayerState, pos: Position, sign: FLT):
    """
    Apply all forces to the p.pos

    :param p: the player state
    :param pos: the player's position
    :param sign: the current sign
    """
    # apply position changes
    pos.ref[0] += sign * (pos.speed[0] if pos.temp_speed[0] is None else pos.temp_speed[0])
    pos.ref[1] += pos.speed[1] if pos.temp_speed[1] is None else pos.temp_speed[1]

    old_sign_x = pos.speed[0] >= 0
    old_sign_y = pos.speed[1] >= 0
    # advance velocity
    pos.speed[0] += pos.force[0] if pos.temp_force[0] is None else pos.temp_force[0]
    pos.speed[1] += pos.force[1] if pos.temp_force[1] is None else pos.temp_force[1]

    new_sign_x = pos.speed[0] >= 0
    new_sign_y = pos.speed[1] >= 0

    if new_sign_x != old_sign_x:
        position_logger.debug("[%s] X sign change", p.char)
        if pos.on_speed_change[0] is not None:
            pos.speed[0] = pos.on_speed_change[0]
            pos.on_speed_change[0] = None
        if pos.on_speed_change[3] is not None:
            pos.force[0] = pos.on_speed_change[3]
            pos.on_speed_change[3] = None
    if new_sign_y != old_sign_y:
        position_logger.debug("[%s] Y sign change", p.char)
        if pos.on_speed_change[1] is not None:
            pos.speed[1] = pos.on_speed_change[1]
            pos.on_speed_change[1] = None
        if pos.on_speed_change[4] is not None:
            pos.force[1] = pos.on_speed_change[4]
            pos.on_speed_change[4] = None

    stun_force = p.stun_force
    if stun_force is not None:
        pos.ref[0] += stun_force.speed[0]
        pos.ref[1] += stun_force.speed[1]
        stun_force.speed[0] += stun_force.force[0]
        stun_force.speed[1] += stun_force.force[1]
        stun_force.t -= 1
        if stun_force.t == 0:
            p.stun_force = None
            p.script.speed = None
            p.script.tick = 0


def reach_ground_transition(p: PlayerState, pos: Position, script: MoveScript) -> Optional[AutoCancel]:
    """
    Apply reach ground transition if there is one

    :param p: the player state
    :param pos: the position
    :param script: the current script
    :param current_y_shift: the absolute position shift for this frame
    :param position_change: the list of position shift entries (to gather the y bound if necessary)
    :return: the optional auto-cancel to transition to
    """
    go_to_next = None
    y_bound = script.position_shifts.get(PositionShifts.Y | PositionShifts.BOUND, p.script.time, ZERO)

    # Warning due to floating point errors, pos.coord[1] < -y_bound is faulty! (Z21 SHOURYU_M)
    if pos.coord[1] + y_bound >= 0:
        return
    previous_vy = pos.speed[1]
    transition_logger.debug("[%s][%s] Reached ground", p.char, p.script)
    # when we land, we always land on the bound and we reset speed
    pos.ref[1] = -y_bound
    pos.coord[1] = -y_bound + pos.shift[1]
    pos.speed[1] = ZERO

    if p.lock_transition:
        return None

    # now for the transition
    # look one step ahead (URN HEADBUTT M/H)
    auto_cancels = script.get_auto_cancels(p.script.time) + script.get_auto_cancels(p.script.time + 1)
    where_to = [b for b in auto_cancels if b.condition == 8]
    if where_to and where_to[0].move_index >= 0:
        # we have an auto-cancel for this
        return where_to[0]
    if script.is_falling() or p.effects is None or script.is_knockdown():
        # we are falling or dropping so we have to declare that we landed
        if script.is_landing_required():
            # this is still mysterious to me
            if 50 <= p.script.script < 400:
                # Recover scripts, actually don't land and can interrupt
                p.script.countdown = 0
            # if it is not a jump, there is a landing to perform
            elif p.script.script not in {5, 8, 11}:
                # transition to landing, select the right one based on pos.speed
                p.queue += [(9 if p.pos.speed[0] > ZERO else 12) if p.pos.speed[0] != ZERO else 6]
            else:
                p.queue += [0]
        elif script.is_knockdown():
            p.queue = [220 + script.hurt_type]
        # get out of this state now!
        go_to_next = object.__new__(AutoCancel)
        go_to_next.condition = 8
        go_to_next.move_index = -1
        go_to_next.time = 0
        go_to_next.params = []
    else:
        # reach ground will be later until then keep the same vy (URN HEADBUTT M/H)
        pos.speed[1] = previous_vy
    return go_to_next


def apply_homing_shifts(pos: Position, others: Dict[Any, Other], sign: FLT, script_id: int, dist_x: FLT, opponent_x: FLT, opponent_y: FLT):
    """
    Apply homing shift situations (homing and camera wall)

    :param pos: the player position
    :param others: the list of other blocs
    :param sign: the sign that represents the sign
    :param script_id: the script id currently applied
    :param dist_x: the distance to the opponent (before the computation)
    """
    if (0, 6) in others:
        o = others[(0, 6)]
        # homing the opponent
        # TODO: figure out the recovery value here
        if script_id == 948:  # HOOLIGAN_EX
            recovery = 28
        elif script_id == 967:  # TC_HOOLIGAN_EX
            recovery = 28  # NEVER TESTED
        elif script_id == 909:  # BAR_JUMP
            recovery = 42
        elif script_id == 917:  # EX_BAR_JUMP
            recovery = 42  # NEVER TESTED
        elif script_id == 762:  # VT_HEADPRESS
            recovery = 31  # NEVER TESTED
        elif script_id == 930:  # HEADPRESS_H
            recovery = 38
        elif script_id == 931:  # HEADPRESS_EX
            recovery = 31
        else:
            recovery = 30
        pos.speed[0] = (FLT(abs(dist_x)) - FLT(o.params[0] / 100)) / FLT(recovery) * sign
    if (0, 8) in others:
        o = others[(0, 8)]
        # homing the camera wall
        camera_target = o.params
        side = FLT(1 if camera_target[2] == 1 else -1)
        # BOUND should be the actual camera wall which is not impl yet
        camera_wall_x = BOUND * side
        dist_until_wall = FLT(camera_wall_x - pos.coord[0] if side else pos.coord[0] - camera_wall_x)
        recovery = FLT(camera_target[0])
        pos.speed[0] = (dist_until_wall + FLT(3)) / recovery * FLT(1 if side else -1)
        vy = FLT(camera_target[1] / 100) * TWO / recovery
        pos.speed[1] = vy
        pos.force[1] = -vy / recovery
    if (0, 12) in others:
        o = others[(0, 12)]
        # position module
        if o.params[0] == 2:
            pos.ref[0] = opponent_x + FLT((1 if pos.side else -1)*(o.params[3]/100))
            pos.ref[1] = opponent_y + FLT(o.params[4]/100)
            pos.coord[0] = pos.ref[0] + pos.shift[0]
            pos.coord[1] = pos.ref[1] + pos.shift[1]
