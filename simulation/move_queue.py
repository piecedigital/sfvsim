from typing import List, Optional, Set

from model import PlayerState
from model.cancel import is_resting_script
from simulation.simulator import Simulator, transition_logger, position_logger


class CouldNotCancel(Exception):
    def __init__(self, message: str):
        super().__init__(message)


class MoveQueueItem:
    def __init__(self, move_name: str, t: Optional[int] = None, cancel_list: Optional[int] = None, hold: Optional[int] = 0):
        self.move_name = move_name
        self.t = t
        self.cancel_list = cancel_list
        self.hold = hold or 0


class MoveQueue:
    def __init__(self, queue: List[MoveQueueItem], resting_position: int):
        self.queue = queue
        self.resting_position = resting_position
        self.first = True
        self.current = None

    def can_transition(self, cancel_lists: Optional[int], raise_miss_cancel=True):
        if cancel_lists is None:
            if self.current is None:
                return True
            if self.current.cancel_list is not None and self.current.cancel_list > 0 and raise_miss_cancel:
                raise CouldNotCancel("Tried to cancel but reached the end of the move %s" % self.current.move_name)
            t = self.current.t
            return t is None or t < 0
        else:
            if self.current is None:
                return False
            if self.current.cancel_list is None:
                return False
            if self.current.cancel_list == cancel_lists:
                if self.current.t <= 0:
                    return True
                self.current.t -= 1
                return False
            return False

    def next_move(self, p: PlayerState):
        if len(self.queue) == 0:
            self.current = None
            return [self.resting_position], 1
        item = self.current = self.queue.pop(0)

        transition_logger.info("[%s][%s] MoveQueue transition to next move %s" %
                               (p.char, p.script, item.move_name))
        return item.move_name, item.hold

    def tick(self):
        if self.first:
            self.first = False
            return True
        if self.current is None:
            return False
        item = self.current
        if item.t is None:
            return False
        if item.cancel_list is not None:
            return False
        item.t -= 1
        return item.t == 0


def execute_simulation(sim: Simulator, p1: PlayerState, queue_p1: MoveQueue, p2: PlayerState, queue_p2: MoveQueue,
                       func, max_frames=1000):
    """
    Main function to execute a simulation

    :param sim: the simulator
    :param p1: the first player state
    :param queue_p1: the first player queue
    :param p2: the second player state
    :param queue_p2: the second player queue
    :param func: the function to apply on each step
    :param max_frames: the maximum number of frames to execute
    """
    frame_count = 0
    p2.recovery = 0
    while max_frames > 0:
        frame_count += 1
        max_frames -= 1
        if queue_p1.tick():
            sim.apply_next(p1, queue_p1.next_move)
        if queue_p2.tick():
            sim.apply_next(p2, queue_p2.next_move)

        sim.step(p1, p2, queue_p1.next_move, queue_p2.next_move, queue_p1.can_transition, queue_p2.can_transition,
                 frame_count=frame_count)

        if func is not None:
            func(p1, p2)

        position_logger.debug(
            "[%.06f, %.06f] %s[%d] [%.06f, %.06f] %s[%d]" % (
                p1.pos.coord[0], p1.pos.coord[1], p1.script, p1.stun if p1.stun is not None else -1, p2.pos.coord[0], p2.pos.coord[1], p2.script, p2.stun if p2.stun is not None else -1
            ))
        is_p1_resting = is_resting_script(p1.char, p1.script.script) and p1.script.time >= 0
        is_p2_resting = is_resting_script(p2.char, p2.script.script) and p2.script.time >= 0
        if not is_p1_resting and not is_p2_resting:
            p2.recovery = 0
        if is_p1_resting:
            p2.recovery += 1
        if is_p2_resting:
            p2.recovery -= 1
        if queue_p1.can_transition(None, raise_miss_cancel=False) and queue_p2.can_transition(None, raise_miss_cancel=False):
            if is_p1_resting and is_p2_resting:
                break
    return frame_count
