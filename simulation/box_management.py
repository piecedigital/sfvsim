from typing import Iterable, List, Optional

from model import ZERO, Box, PlayerState, T, MoveScript, FLT, Hitbox, PositionShifts
from .log import hit_logger


def range_intersection(x1: FLT, w1: FLT, x2: FLT, w2: FLT, side: Optional[bool]=None):
    """
    Return the range intersection of [x1, x1+w1] and [x2, x2+w2]
    :return: the size of the intersection, 0 if it doesnt intersect
    """
    side_to_consider = x1 <= x2
    if side is not None:
        side_to_consider = side
    if side_to_consider:
        result = x1 + w1 - x2
    else:
        result = x2 + w2 - x1
    return max(ZERO, result)


def box_collide(b1: Box, b2: Box):
    """
    :return: True if boxes given as parameter intersect each other
    """
    return range_intersection(b1.x, b1.width, b2.x, b2.width) > 0 and \
           range_intersection(b1.y, b1.height, b2.y, b2.height) > 0


def compute_physical_box(p: PlayerState, script: MoveScript):
    time = p.script.time
    x_shift = script.position_shifts.get(PositionShifts.X | PositionShifts.HITBOX_SHIFT, time, ZERO)
    y_shift = script.position_shifts.get(PositionShifts.Y | PositionShifts.HITBOX_SHIFT, time, ZERO)
    p.phys = compute_boxes(p, script.get_physics_boxes(time), x_shift, y_shift)
    return


def compute_hitboxes(p: PlayerState, all_boxes: List[Hitbox], shift_x: FLT, shift_y: FLT) -> List[Hitbox]:
    if not all_boxes:
        return all_boxes
    x, y = p.pos.coord
    side = p.pos.side
    y += shift_y
    if side:
        x += shift_x
    else:
        x -= shift_x
    boxes = []
    for h in all_boxes:
        # If it is a proximity box, just ignore it
        if h.type == 4 or h.type > 5:
            continue

        # If it is a reset box
        if h.reset_box() and p.effects is not None:
            if p.stun is not None:
                continue
            # If we have used the hitbox before, remove it
            if h.get_id() in p.script.used_hitboxes:
                p.script.used_hitboxes.remove(h.get_id())

            if p.script.time == h.tick_start:
                # On the first frame, remove the hit box
                if h.get_id() in p.script.hit_hitboxes:
                    p.script.hit_hitboxes.remove(h.get_id())
            elif h.get_id() in p.script.hit_hitboxes:
                # On other frames, dont show it if we have it
                continue
        else:
            # If we have used the hitbox, get out
            if h.get_id() in p.script.used_hitboxes:
                continue

        if p.opponent.juggle_state == "RESET":
            hit_logger.debug("Ignoring box due to juggle reset")
            continue
        if p.opponent.juggle_state == "JUGGLE" and h.juggle_limit < p.opponent.juggle:
            hit_logger.debug("Ignoring box due to juggle limit=%d < juggle=%d", h.juggle_limit,
                            p.opponent.juggle)
            continue

        if side:
            h.x += x
        else:
            h.x = x - h.x - h.width
        h.y += y
        boxes.append(h)
    return boxes


def compute_boxes(p: PlayerState, all_boxes: List[T], shift_x: FLT, shift_y: FLT) -> List[T]:
    if not all_boxes:
        return all_boxes
    x, y = p.pos.coord
    side = p.pos.side
    y += shift_y
    if side:
        x += shift_x
        for h in all_boxes:
            h.x += x
            h.y += y
    else:
        x -= shift_x
        for h in all_boxes:
            h.x = x - h.x - h.width
            h.y += y
    return all_boxes
