from collections import defaultdict
from typing import Dict

from model import ZERO, FLT, TWO, Position, Hitbox, PlayerState, MoveScript, SFVData, Hurtbox, is_resting_script, \
    Optional, StunForce, Set, List, Tuple
from .box_management import box_collide
from .log import hit_logger


def apply_trade_system(data: SFVData, p1: PlayerState, p2: PlayerState, random_bool):
    """
    Apply the trading system as described in https://twitter.com/WydD/status/911725965453250560

    Will maybe remove one element from the hit_queue of one

    :param data: the SFVData to extract the move description when necessary
    :param p1: the player 1 state
    :param p2: the player 2 state
    :param random_bool: a random bool (usually equal to frame_counter % 2 == 1)
    """
    # make sure that we select the first hitbox
    if not p1.hit_queue:
        for p in p1.effects:
            if p.hit_queue:
                p1 = p
                break
    if not p2.hit_queue:
        for p in p2.effects:
            if p.hit_queue:
                p2 = p
                break
    # No trade to apply
    if not p1.hit_queue or not p2.hit_queue:
        return
    p1_hit = p1.hit_queue[0][0]
    p2_hit = p2.hit_queue[0][0]
    if p1_hit.type == p2_hit.type:
        if p1_hit.type == 0:
            p1_move = data.get_current_move_description(p1)
            p2_move = data.get_current_move_description(p2)
            if p1_move is None or p2_move is None:
                # should not happen but it's a prevention
                hit_logger.warn("Unknown situation happened: priority system encountered a null move description")
                return
            # normal or special against each other
            if p1_move.type < 3 and p2_move.type < 3 and not p1.status_air and not p2.status_air:
                # normal vs normal
                if (p1_move.properties & 0x7) == (p2_move.properties & 0x7):
                    # pure trade
                    return
                # apply priority and force counter-hit
                if (p1_move.properties & 0x7) < (p2_move.properties & 0x7):
                    p1.hit_queue.pop(0)
                    p1.status_counter = True
                else:
                    p2.hit_queue.pop(0)
                    p2.status_counter = True
                return
            # Other situations are trade
            return
        if p1_hit.type == 2:
            # Throw
            # at random invert p1 and p2
            if random_bool:
                p1, p2 = p2, p1
            # the attacker wins
            p2.hit_queue.pop(0)
        # Projectiles between themselves, let them trade
        return
    if p1_hit.type > p2_hit.type:
        # to avoid multiple condition, switch p1 & p2 to guarantee p1.type < p2.type
        p1, p2 = p2, p1
        p1_hit, p2_hit = p2_hit, p1_hit
    if p1_hit.type == 0:
        # strike against throw or projectile
        if p2_hit.type == 1:
            # standard trade against projectile
            return
        # against throws, the strike loses
        p1.hit_queue.pop(0)
        return
    if p1_hit.type == 1:
        # projectiles against any kind of throws
        # throws loses
        p2.hit_queue.pop(0)
        return
    return


def projectile_trade(data: SFVData, p1: PlayerState, p2: PlayerState):
    """
    Perform the projectile trade step with absorbtion and reflect as well

    :param data: The SFV Data used for type5 hitboxes
    :param p1: The first player
    :param p2: The second player
    """
    all_p1_projectiles = [(h, p1) for h in p1.hit if h.type == 1]
    all_p1_projectile_effects = [(h, e) for e in p1.effects for h in e.hit if h.type == 1]
    all_p2_projectiles = [(h, p2) for h in p2.hit if h.type == 1]
    all_p2_projectile_effects = [(h, e) for e in p2.effects for h in e.hit if h.type == 1]

    consumed_boxes = defaultdict(lambda: set())
    # first pass, make the absorb and reflect work
    apply_type5(data, p1, all_p2_projectile_effects, consumed_boxes)
    apply_type5(data, p2, all_p1_projectile_effects, consumed_boxes)

    # second pass, make the projectile collide
    for ph1, ps1 in all_p1_projectiles + all_p1_projectile_effects:
        if ph1.get_id() in consumed_boxes[ps1]:
            continue
        for ph2, ps2 in all_p2_projectiles + all_p2_projectile_effects:
            if ph2.get_id() in consumed_boxes[ps2]:
                continue
            if box_collide(ph1, ph2):
                consumed_boxes[ps1].add(ph1.get_id())
                consumed_boxes[ps2].add(ph2.get_id())
                consume_hitbox(ps1, ph1)
                consume_hitbox(ps2, ph2)
                hit_logger.info(
                    "[%s][%s] - [%s][%s] Traded projectile %d - %d",
                    ps1.char, ps1.script, ps2.char, ps2.script, ph1.get_id(), ph2.get_id()
                )
                break

    # final pass, remove all used boxes
    for ps, hitboxes in consumed_boxes.items():
        if not hitboxes:
            continue
        ps.hit = [h for h in ps.hit if h.get_id() not in hitboxes]


def apply_type5(data: SFVData, p: PlayerState, opponent_projectile_effects: List[Tuple[Hitbox, PlayerState]], consumed_boxes: Dict[PlayerState, Set[int]]):
    """
    Apply the hitbox type 5 collision and effect

    :param data: The SFV Data
    :param p: The main player state to apply
    :param opponent_projectile_effects: The list of opponent hitbox+playerstate
    :param consumed_boxes: The dictionary of consumed hitboxes
    """
    # First: gather all the hitboxes
    all_type5 = [
        (h, p, data.get_hitbox_effect(p.current_char(), h.effect_index, "GUARD", "STAND"))
        for h in p.hit if h.type == 5
    ] + [
        (h, e, data.get_hitbox_effect(e.current_char(), h.effect_index, "GUARD", "STAND", True))
        for e in p.effects
        for h in e.hit if h.type == 5
    ]
    """:type: List[Tuple[Hitbox, PlayerState, HitboxEffect]]"""

    # Now do the collision
    ignore_effects = set()
    for ph1, ps1, he in all_type5:
        if ph1.get_id() in consumed_boxes[ps1]:
            continue
        for ph2, ps2 in opponent_projectile_effects:
            if ps2 in ignore_effects:
                continue
            if ph2.get_id() in consumed_boxes[ps2]:
                continue
            if ph1.get_priority() < ph2.get_priority():
                # we have to check if the hit_level is high enough to perform the collision as expected
                continue
            # collide
            if box_collide(ph1, ph2):
                # now we execute the effect
                # first the stun
                if he.hitstun_attacker > 0:
                    ps1.stun = he.hitstun_attacker & 511
                ps2.stun = max(1, he.hitstun_defender & 511)
                # absorb the box
                if he.knock_back > 0:
                    # reflect
                    # transfer the effect to the other player
                    ps1.opponent.opponent.effects.append(ps2)
                    ps2.opponent.opponent.effects.remove(ps2)
                    # invert the forces (not the side because the hitbox shape must stay the same)
                    ps2.pos.speed[0] = -ps2.pos.speed[0] * he.knock_back
                    ps2.pos.force[0] = -ps2.pos.force[0] * he.knock_back
                    # change the opponent
                    ps2.opponent = ps2.opponent.opponent
                    consume_hitbox(ps1, ph1)
                    consumed_boxes[ps2].update({h.get_id() for h in ps2.hit})
                    hit_logger.info(
                        "[%s][%s] Reflecting projectile %d using %d[knockback=%f, stun=%d]",
                        ps1.char, ps1.script, ph2.get_id(), he.index, he.knock_back, he.hitstun_defender
                    )
                else:
                    # absorb the hit like a standard projectile vs projectile
                    consume_hitbox(ps1, ph1)
                    consume_hitbox(ps2, ph2)
                    consumed_boxes[ps1].add(ph1.get_id())
                    consumed_boxes[ps2].add(ph2.get_id())
                    hit_logger.info(
                        "[%s][%s] Absorbed projectile %d using %d[knockback=%f, stun=%d]",
                        ps1.char, ps1.script, ph2.get_id(), he.index, he.knock_back, he.hitstun_defender
                    )
                break


def perform_hit_collision(p: PlayerState):
    """
    Apply hit collision based on p.hit hitboxes against the opponent hurtboxes while respecting box properties.

    Will apply do_box_collide on each collision (even if multiple hitboxes hit)

    :param p:
    :return:
    """
    for h in p.hit:
        if h.type >= 4:
            continue
        if p.opponent.script.temp_char is not None and p.effects is not None:
            enqueue_box_collide(p, h, None)
            continue
        if h.cant_hurt_air() and p.opponent.status_air:
            continue
        if h.cant_hurt_crouched() and p.opponent.status_crouched:
            continue
        if h.cant_hurt_stand() and (not p.opponent.status_crouched and not p.opponent.status_air):
            continue
        # okay... what's this?
        if h.knockdown & 512:
            continue
        for h2 in sorted(p.opponent.hurt, key=Hurtbox.get_collision_priority):
            if h.type == 0 and h2.type >= 4:
                continue
            if h.type == 2 and h2.type < 4:
                continue
            if h.type == 1 and h2.type & 2 == 0:
                continue
            if h2.air_inv and p.status_air:
                continue
            if box_collide(h, h2):
                if not h2.is_armor:
                    enqueue_box_collide(p, h, h2)
                    if h.get_id() != 255:
                        return
                    break
                if armor_hit(p, h, h2):
                    break


def armor_hit(p: PlayerState, hit: Hitbox, hurt: Hurtbox):
    """
    Try to armor a hit with an armored hurtbox.

    :param p: the player state
    :param hit: the hitbox to armor
    :param hurt: the hurtbox
    :return: true if the hit has been absorbed
    """
    armor_group = hurt.get_armor_group()
    if armor_group not in p.opponent.armor:
        p.opponent.armor[armor_group] = []
    armor = p.opponent.armor[armor_group]
    if len(armor) >= hurt.armor_count:
        # we have exceed the armor limit
        # we can safely ignore this armor hurtbox
        return False
    # freeze me because I've been armored for the amount of time required by the armor (if any)
    p.stun = (p.stun or 0) + hurt.get_armor_freeze()
    # we have indeed armored the hit
    armor.append(hit)
    # do consume the hit nonetheless
    consume_hitbox(p, hit)
    return True


def consume_hitbox(p, hit):
    """
    Consume a hitbox in the script

    :param p: the player state
    :param hit: the hitbox to consume
    """
    if not hit.reset_box() or p.effects is None:
        p.script.used_hitboxes.add(hit.get_id())
    p.script.hit_hitboxes.add(hit.get_id())


def enqueue_box_collide(p: PlayerState, h: Hitbox, hurt: Optional[Hurtbox]):
    """
    Enqueue the box for actual collision.

    The box is put on the hit queue only if it's not already there.
    If the number of hits is greater than one, than the hitbox is applied more than once.

    :param p: the state of the player hitting
    :param h: the box
    :param hurt: the target hurtbox
    """
    # check if a similar hitbox is already in the queue
    if h.get_id() != 255:
        for hb, hurt in p.hit_queue:
            if hb.get_id() == h.get_id():
                consume_hitbox(p, h)
                return
    # append the hitbox to the queue
    for _ in range(max(1, h.number_of_hits & 255)):
        p.hit_queue.append((h, hurt))
    consume_hitbox(p, h)


def apply_hit_queue(data: SFVData, p: PlayerState, hitbox: Hitbox, hurtbox: Optional[Hurtbox]):
    """
    Actually execute the hit collision.

    :param data: the data
    :param p: the player state
    :param hitbox: the hitbox that has stroke
    :param hurtbox: the hurtbox that has been struck
    """
    effect_index = hitbox.effect_index
    juggle_increase = hitbox.juggle_increase
    side = FLT(1 if p.opponent.opponent.pos.coord[0] < p.opponent.pos.coord[0] else -1)
    if 220 <= p.opponent.script.script < 230:
        effect = data.get_hitbox_effect(p.current_char(), effect_index, "HIT", "UNKNOWN", p.effects is None)
        # only type 4 will hit otg
        if effect.type != 4:
            # ignore the box, i'm pretty sure this method will backfire at me some day
            p.script.hit_hitboxes.remove(hitbox.get_id())
            return
        hit_effect = "HIT"
        opponent_status = "UNKNOWN"
    else:
        opponent_status = "AIR" if p.opponent.status_air else ("CROUCH" if p.opponent.status_crouched else "STAND")

        hit_effect = p.opponent.hit_effect
        if hit_effect == "GUARD2ND":
            hit_effect = "HIT"
            p.opponent.hit_effect = "GUARD"
        if hit_effect == "GUARD":
            if not (is_resting_script(p.opponent.char, p.opponent.script.script) or 50 <= p.opponent.script.script < 120):
                # Cant guard if not resting or already guarding
                hit_effect = "HIT"
            elif opponent_status == "AIR":
                # Cant guard in the air
                hit_effect = "HIT"
            elif hitbox.type == 2:
                # Cant guard a throw
                hit_effect = "HIT"

        if hit_effect == "HIT" and p.opponent.status_counter and p.opponent.opponent.combo_counter == 0:
            hit_effect = "COUNTERHIT"
        effect = data.get_hitbox_effect(p.current_char(), effect_index, hit_effect, opponent_status, p.effects is None)
    recovery = effect.recovery_frames
    hit_animation = effect.index
    if not effect.is_specific_script():
        if opponent_status == "AIR":
            hurt_type = 4
        elif opponent_status == "CROUCH":
            hurt_type = 3
        elif hurtbox is not None:
            hurt_type = hurtbox.hurt_type
        else:
            hurt_type = 0
        # force hurt type to be correct
        hit_range = hitbox.hit_range >> 4
        if effect.type == 1:
            if effect.is_crush():
                hit_animation = 170 + hurt_type * 3 + hit_range
            else:
                hit_animation += 120 + hurt_type * 9 + hit_range * 3
        elif effect.type == 2:
            hit_animation += 70 + hurt_type * 9 + hit_range * 3
        elif effect.type == 3:
            hit_animation += 210
        elif effect.type == 4:
            hit_animation += 220
    if effect.is_hard_kd():
        p.opponent.knockdown_type = "HARD"
    elif hitbox.knockdown & 16:
        p.opponent.knockdown_type = "STAND"
    else:
        p.opponent.knockdown_type = "NORMAL"
    if 220 <= p.opponent.script.script < 230:
        p.opponent.change_script(hit_animation, 0, True)
    else:
        p.opponent.queue = []
    p.opponent.corner_push_opponent = False
    target_script = data.get_current_script(p, hit_animation)
    p.opponent.script.speed = None
    falling = target_script.is_falling() or target_script.is_landing_required()
    if 1 <= effect.type < 3 and not falling and recovery > 0:
        p.opponent.corner_push_opponent = not p.status_air
        vx = FLT(effect.knock_back) * TWO / FLT(recovery)
        p.opponent.stun_force = StunForce()
        p.opponent.stun_force.speed[0] = side * vx
        p.opponent.stun_force.force[0] = - side * vx / FLT(recovery)
        p.opponent.stun_force.t = recovery
        frame_to_target = target_script.last_hitbox_frame if target_script.last_hitbox_frame > 0 else target_script.get_script_length()
        speed = int(65536*frame_to_target/recovery)
        p.opponent.script.speed = [speed, (65536 - speed*recovery % 65536) % 65536, recovery]
    elif effect.type == 3 or (effect.type == 1 and falling):
        vx = FLT(effect.knock_back) / FLT(recovery)
        p.opponent.stun_force = StunForce()
        if effect.fall_speed > 0:
            y_recovery = FLT(recovery) / TWO
            vy = FLT(effect.fall_speed) * TWO / y_recovery
            p.opponent.stun_force.force[1] += -FLT(vy) / y_recovery
        else:
            # If fall_speed is negative
            # Rules are not the same \o/
            vy = FLT(effect.fall_speed) / FLT(recovery)
        p.opponent.stun_force.speed[0] += side * vx
        p.opponent.stun_force.speed[1] += vy
        p.opponent.stun_force.t = 10000
    elif effect.type == 4:
        if opponent_status == "UNKNOWN" and p.opponent.stun_force:
            # OTG push back = slow down the current script
            p.opponent.stun_force.speed[0] /= FLT(5)
            p.opponent.stun_force.force[0] = ZERO
            p.opponent.stun_force.t = 10000
    elif effect.type == 0:
        pass
    if hitbox.type != 2:
        p.stun = effect.hitstun_attacker & 511
        p.opponent.stun = effect.hitstun_defender & 511
    if 0 < effect.type and 0 < hit_animation:
        p.opponent.change_script(hit_animation, 0, True)
        # reset vy ay
        p.opponent.pos.speed[1] = ZERO
        p.opponent.pos.force[1] = ZERO
        if effect.type == 1:
            if p.opponent.juggle_state == "JUGGLE":
                p.opponent.juggle_state = "RESET"
                p.opponent.juggle += juggle_increase
            elif opponent_status == "AIR":
                p.opponent.juggle_state = "RESET"
                p.opponent.juggle += juggle_increase
        elif effect.type == 2:
            p.opponent.juggle_state = "NONE"
            p.opponent.juggle = 0
        elif effect.type == 3:
            if p.opponent.juggle_state == "JUGGLE":
                p.opponent.juggle += juggle_increase
            else:
                p.opponent.juggle_state = "JUGGLE"
                p.opponent.juggle = effect.juggle_start
    hit_logger.info(
        "[%s][%s] Has hit opponent[%s, %s] with %d[%d, recovery=%d stun=%d animation=%d downtime=%d]",
        p.char, p.script, hit_effect, opponent_status, effect_index, effect.type, recovery, p.stun if p.stun is not None else -1,
        hit_animation,
        effect.downtime
    )
    p.opponent.downtime = effect.downtime
    if effect.damage > 0:
        if hit_effect != "GUARD":
            p.opponent.opponent.combo_counter += 1
        else:
            p.opponent.opponent.combo_counter = 0
    p.script.has_hit += (-1 if hit_effect == "GUARD" else 1)
    p.script.hit_status = (2 if hit_effect == "GUARD" else 1)

