import random
from functools import reduce

from typing import Optional, Set, Tuple
from model import ZERO, FLT, PlayerState, Position, SFVData, MoveScript, AutoCancel, is_resting_script, PositionShifts, \
    is_walking_script
from .log import position_logger, transition_logger


class InfiniteTransitionException(Exception):
    pass


def main_transition(data: SFVData, p: PlayerState, apply_next, can_transition, auto_cancel_used: Set[AutoCancel],
                    rest_transition: bool = True, recursion_count=0) -> Optional[MoveScript]:
    """
    Transition step: first step of the process, it will select the right script to execute

    :param data: the sfv data
    :param p: the player
    :param apply_next: function called when the next move needs to be queued
    :param can_transition: function that returns true when a move is available in apply_next
    :param rest_transition: if False the transition step cannot transition to a resting state (avoid infinite recursion)
    :return: the script that will be used or None if no script will be executed this frame (stun for instance)
    """
    if recursion_count > 10:
        raise InfiniteTransitionException("Internal Error: Exceeded recursion limit in %s" % str(p.script))

    if p.stun is not None:
        if p.stun <= 0:
            p.stun = None
        else:
            p.stun -= 1
            if not p.script.init:
                return None

    # Because it is too hard to sync with actual data
    # Every rest position stays at 0
    resting_script = is_resting_script(p.char, p.script.script) if p.effects is not None else False
    if resting_script:
        if p.stun_force is not None:
            p.stun_force = None
        p.opponent.combo_counter = 0
        if p.effects is not None:
            p.script.time = 0
            p.script.init = True
            # If we stand we can turn either way
            p.pos.side = p.pos.coord[0] < p.opponent.pos.coord[0]
        # Reset juggle state
        p.juggle_state = "NONE"
        p.juggle = 0

    script = data.get_current_script(p)
    if not p.script.init or resting_script or is_walking_script(p.char, p.script.script):
        cancel_buffer = script.get_cancel_buffers(p.script.time)
        if cancel_buffer:
            # update buffered cancels
            for cl, ctype in cancel_buffer.items():
                if can_transition(cl):
                    p.buffered_cancels[cl] = ctype
        if p.buffered_cancels:
            # try to launch
            hit_status = p.hit_status(script.last_hitbox_frame)
            cancels = script.get_cancel_lists(p.script.time)
            valid_cancel = False
            for cl, ctype in p.buffered_cancels.items():
                if cl not in cancels:
                    continue
                if ctype == 0 or ctype & hit_status:
                    valid_cancel = True
                    break
            if valid_cancel:
                apply_next(p)
                return main_transition(data, p, apply_next, can_transition, auto_cancel_used, recursion_count=recursion_count+1)

    if p.effects is None and p.script.time >= 0:
        # If this projectile has no more hitboxes, do a transition
        latest_hitbox = script.latest_hitbox
        if latest_hitbox is not None and latest_hitbox < p.script.time and not p.hit:
            transition_logger.info("[%s][%s] Effect has no more hitbox", p.char, p.script)
            if can_transition(None):
                apply_next(p)
                return main_transition(data, p, apply_next, can_transition, auto_cancel_used, recursion_count=recursion_count+1)

    if resting_script:
        p.move_id = None
        if rest_transition:
            p.corner_push_opponent = False
            if can_transition(None):
                apply_next(p)
                # Avoid infinite recursion
                return main_transition(data, p, apply_next, can_transition, auto_cancel_used, rest_transition=False, recursion_count=recursion_count+1)

    # TODO Relax this condition when #25 is fixed
    total_ticks = script.total_ticks if script.interrupt_frame < 0 or (script.interrupt_frame == 0 and script.index < 30) else min(script.total_ticks,
                                                                            script.interrupt_frame + 1)

    force_finish = False
    if script.interrupt_frame > 0 and not script.is_landing_required():
        if p.script.time == script.interrupt_frame:
            transition_logger.info("[%s][%s] Forcing script to interrupt", p.char, p.script)
            force_finish = True

    p.script.previous_time = p.script.time
    if p.script.init:
        if script.position_shifts.get(PositionShifts.X, p.script.time, None) is None:
            p.reset_shift()
        p.pos.force[0] *= FLT(script.slide_acc)
        p.pos.force[1] *= FLT(script.slide_y_acc)
        p.pos.speed[0] *= FLT(script.slide)
        p.pos.speed[1] *= FLT(script.slide_y)
        if script.is_on_ground() and p.effects is not None:
            p.pos.ref[1] = ZERO
            p.juggle_state = "NONE"
            p.juggle = 0
        p.script.init = False
    else:
        if p.script.speed:
            p.script.speed[2] -= 1
            new_tick = p.script.tick + p.script.speed[0]
            if p.script.speed[2] < p.script.speed[1]:
                new_tick += 1
            p.script.tick = new_tick % 65536
            p.script.time += new_tick // 65536
            if p.script.speed[2] == 0:
                p.script.speed = None
                p.script.tick = 0
                if p.script.time >= script.get_script_length():
                    p.script.time = script.get_script_length() - 1
        else:
            p.script.tick = 0
            p.script.time += 1

    if script.is_bound():
        p.script.countdown = None
        if p.script.time == 0:
            if p.stun_force is not None:
                # dirty, should not be done here I think
                if script.slide > 0.0:
                    p.stun_force.speed[0] *= FLT(script.slide * 2)
                    p.stun_force.force[0] = FLT(-p.stun_force.speed[0] / script.total_ticks)
                    p.pos.force[1] = p.pos.speed[1] = ZERO
                    p.stun_force.t = script.total_ticks
                else:
                    p.stun_force = None

        # Bound cancel on 2nd frame
        if p.script.time == 1 and 220 <= p.script.script < 230:
            offset_d = 1 if p.script.script >= 223 else 0
            if p.rise == "NONE" or p.knockdown_type == "HARD":
                p.queue = [235 + offset_d, 240 + offset_d]
            else:
                p.pos.side = p.pos.coord[0] < p.opponent.pos.coord[0]
                p.stun_force = None
                p.select_scripts \
                    ([246 + offset_d if p.rise == "QUICK" or p.knockdown_type == "STAND" else 244 + offset_d])
                return main_transition(data, p, apply_next, can_transition, auto_cancel_used, recursion_count=recursion_count+1)

    if p.script.countdown == 0:
        transition_logger.info("[%s][%s] Forcing script finish due to countdown", p.char, p.script)
        force_finish = True
        p.script.countdown = None
    if p.auto_cancel and p.auto_cancel.move_index < 0:
        transition_logger.info("[%s][%s] Forcing script to finish due to auto cancel %d", p.char, p.script, p.auto_cancel.condition)
        p.auto_cancel = None
        force_finish = True

    if p.script.time >= total_ticks or force_finish:
        p.script.tick = 0
        if not force_finish:
            auto_cancel_land = [b.tick_start for b in script.get_auto_cancels(p.script.time - 1) if b.condition == 8]
            falling = script.is_falling() or auto_cancel_land
            falling = falling and not [s for s in script.get_states(p.script.time - 1) if s.is_on_ground()]
            if p.script.countdown is not None:
                if p.script.countdown > 0:
                    transition_logger.debug("[%s][%s] Looping script to t=0 due to countdown",
                                            p.char, p.script)
                    p.script.time = 0
                    return main_transition(data, p, apply_next, can_transition, auto_cancel_used, recursion_count=recursion_count+1)
            elif falling and (p.script.temp_char is None or p.lock_transition):
                transition_logger.debug("[%s][%s] Looping to t=original_position due to falling", p.char, p.script)
                p.script.time = total_ticks - 2
                p.disable_positions = True
                return main_transition(data, p, apply_next, can_transition, auto_cancel_used, recursion_count=recursion_count+1)

        if p.script.time < script.total_ticks:
            # Dirty but all my other way are buggy
            # #22 if we get out of a script by interruption, apply the Always auto cancel if available
            auto_cancel_always = [b for b in script.get_auto_cancels(script.total_ticks - 1) if b.condition == "Always"]
            if len(auto_cancel_always):
                transition_logger.debug("[%s][%s] Placing script %s in queue because of AutoCancel on interrupt",
                                        p.char, p.script, auto_cancel_always[0].move_index)
                p.queue = [auto_cancel_always[0].move_index] + p.queue
        if script.is_bound():
            p.stun_force = None
        if script.is_knockdown():
            p.queue = [220 + script.hurt_type]
        if p.queue:
            if p.queue[0] >= 400:
                # Mostly useless because we should not have move scripts in queue
                # Should be a rest transition
                p.pos.side = p.pos.coord[0] < p.opponent.pos.coord[0]
            transition_logger.info("[%s][%s] Transitioning to next queued script %s",
                                   p.char, p.script, p.queue[0])
            p.select_scripts(p.queue)
            p.script.time = 0
            if p.script.script == 235 or p.script.script == 236:
                p.script.countdown = p.downtime
            return main_transition(data, p, apply_next, can_transition, auto_cancel_used, recursion_count=recursion_count+1)
        # Dont interrupt if we have nothing better to do!
        transition_logger.info("[%s][%s] Transitioning to rest position", p.char, p.script)
        if p.effects is None:
            if can_transition(None):
                apply_next(p)
            else:
                return None
        else:
            p.select_scripts([0])
        return main_transition(data, p, apply_next, can_transition, auto_cancel_used, recursion_count=recursion_count+1)

    if p.auto_cancel:
        go_to_next = p.auto_cancel
        p.auto_cancel = None
    else:
        go_to_next = get_auto_cancel_transition(p, script, auto_cancel_used)

    if go_to_next is not None:
        auto_cancel_used.add(go_to_next)
        recursion = apply_auto_cancel(p, go_to_next)
        if recursion:
            if p.script.speed:
                # recompute script speed
                current_script = data.get_current_script(p)
                recovery = (p.stun_force.t - 1)
                speed = int(65536 * (current_script.get_script_length() - go_to_next.time) / recovery)
                p.script.speed = [speed, recovery - (65536 - speed*recovery % 65536) % 65536, recovery]
            return main_transition(data, p, apply_next, can_transition, auto_cancel_used, recursion_count=recursion_count+1)
        if p.script.time >= total_ticks:
            p.script.time = total_ticks - 1

    apply_transition_step_others(p, script)

    p.hold -= 1

    return script


def apply_auto_cancel(p: PlayerState, go_to_next: AutoCancel):
    if go_to_next.move_index < 0:
        p.script.time = go_to_next.time
        transition_logger.info("[%s][%s] AutoCancel condition=%s to itself, placing t=%d", p.char, p.script,
                               go_to_next.condition, p.script.time)
        return False
    if go_to_next.transition & 2:
        # continue seamlessly
        p.change_script(go_to_next.move_index, p.script.time, True)
        transition_logger.info("[%s][%s] AutoCancel condition=%s to script %d without changing time",
                               p.char, p.script, go_to_next.condition, go_to_next.move_index)
        return p.script.init
    desired_time = go_to_next.time if go_to_next.move_index >= 0 and go_to_next.time >= 0 else 0
    transition_logger.info("[%s][%s] AutoCancel condition=%s to script %d at time %d",
                           p.char, p.script, go_to_next.condition, go_to_next.move_index, desired_time)
    reset_position_shift = desired_time <= 0
    # This is a stupid condition to handle mujin_h and CMN RYU_HEADBUTT
    # dont hesitate to change this (though I dont know what for)
    if go_to_next.condition == "Always" and not go_to_next.params:
        reset_position_shift = False
    p.change_script(go_to_next.move_index, desired_time, reset_position_shift)
    return p.script.init


def apply_transition_step_others(p: PlayerState, script: MoveScript):
    others = script.get_others(p.script.time)
    if not others:
        return
    if (0, 9) in others:
        other = others[(0, 9)]
        if p.script.time == 0 and other.params[0] <= 0:
            position_logger.info("[%s][%s] Resetting position due to 'other' = (0,9)", p.char, p.script)
            p.pos = Position(0, 0)
    if (0, 1) in others:
        other = others[(0, 1)]
        # stance change
        p.stance = other.params[0]
    if (0, 3) in others:
        other = others[(0, 3)]
        # spawn projectile
        script_id = other.params[0]
        transition_logger.info("[%s][%s] Spawning projectile %d", p.char, p.script, script_id)
        side = FLT(1 if p.pos.side else -1)
        effect = PlayerState(p.char, p.pos.ref[0] + side * FLT(other.params[1] / 100),
                             p.pos.ref[1] + FLT(other.params[2] / 100))
        effect.move_id = p.move_id
        # Disable effects
        effect.effects = None
        effect.opponent = p.opponent
        effect.select_scripts([script_id])
        p.effects.append(effect)
        effect.pos.side = p.pos.side
        effect.script.init = True


def get_auto_cancel_transition(p: PlayerState, script: MoveScript, used) -> Optional[AutoCancel]:
    # Resolve auto cancels
    # It should not need p.script.time - 1... feels like a SFV BUG
    # but that's the only way I support z21-gnd-tatsu-lk.json
    auto_cancels = script.get_auto_cancels(p.script.time - 1) + script.get_auto_cancels(p.script.time)
    if not auto_cancels:
        return
    script_effects = set()
    if not script.others.empty:
        script_effects = {o.params[0] for o in script.others.instances if o.type == (0, 3) and o.tick_start < p.script.time}
    hold_transition = []
    all_hitboxes = set(p.script.hit_hitboxes)
    has_hit = p.script.has_hit > 0
    for ac in auto_cancels:
        if ac in used:
            continue
        # We had one collision
        # Test the hit_transitions
        if script_effects and (ac.condition == 1 or ac.condition == "OnBlock" or ac.condition == 3 or ac.condition == 4 or ac.condition == 5):
            for eff in p.effects or []:
                if eff.script.script not in script_effects:
                    continue
                all_hitboxes.update(eff.script.hit_hitboxes)
                has_hit |= eff.script.has_hit > 0
        if all_hitboxes and ac.can_hit_transition(has_hit, all_hitboxes):
            # accumulate in a hit_transition variable because we want the last one
            return ac
        if not p.script.hit_hitboxes and ac.condition == 3:
            # we have a condition on hitboxes, yet we have none that has hit
            return ac
        # Corner transition
        if ac.condition == 9 and (p.pos.coord[0] >= (7.5 - ac.params[0]) or p.pos.coord[0] <= -(7.5 - ac.params[0])):
            return ac
        # Proximity transition
        if ac.condition == 12:
            if ac.params[0] & 1:
                # distance condition
                if abs(p.pos.coord[0] - p.opponent.pos.coord[0]) < (0.25 + ac.params[1] / 100):
                    # BUGSFV?
                    # CMY V_SKILL close enough skips one frame for some reason
                    if abs(p.pos.coord[0] - p.opponent.pos.coord[0]) > (ac.params[1] / 100):
                        p.script.time -= 1
                    return ac
            else:
                if abs(p.pos.coord[0] - p.opponent.pos.coord[0]) > (0.25 + ac.params[1] / 100):
                    return ac
        # Projectile presence transition
        if ac.condition == 26 and p.effects:
            return ac
        # Resource presence transition
        if ac.condition == 20:
            # V-Trigger
            if ac.params[1] == 2 and p.vtrigger > 0:
                return ac
            # params[1] == 1 -> no more kunais (let's forget this one for now)
            continue
        # Button hold
        if ac.condition == 24 and p.hold > 0:
            # accumulate than randomize
            hold_transition.append(ac)
        if ac.condition == 14:
            # params is [condition, buttons, ?, ?]
            # hold all buttons
            if ac.params[0] == 0 and p.hold > 0:
                return ac
            # interrupt with buttons (BRD CHAIN)
            if ac.params[0] == 1:
                # not supported
                continue
            # release any or release both
            if ac.params[0] == 2 or ac.params[0] == 3:
                if p.hold <= 0:
                    return ac
                continue
            # hold any button
            if ac.params[0] == 4 and p.hold > 0:
                return ac

        if ac.condition in {16, 17} and p.armor:
            sub_condition_mask = ac.params[0] if ac.params else 0
            if sub_condition_mask:
                all_hitboxes = [(h.hit_level << 1) or 1 for armored in p.armor.values() for h in armored]
                hit_mask = reduce(lambda a, b: (a | b), all_hitboxes, 0)
                # we now have hit mask with: 1 = mid, 2 = high, 4 = low
                # unfortunately, condition mask is 1 = low, 2 = mid, 4 = high
                # here's the alignment
                hit_mask = ((hit_mask << 1) | (hit_mask >> 2)) & 7
                if hit_mask & sub_condition_mask:
                    return ac
            else:
                return ac

        if ac.condition == "Always" and ac.tick_start == p.script.time:
            return ac

    if hold_transition:
        return hold_transition[random.randrange(0, len(hold_transition))]

    return None


def get_auto_cancel_transition_post_hit(p: PlayerState, script: MoveScript) -> Optional[AutoCancel]:
    auto_cancels = script.get_auto_cancels(p.script.time)
    if not auto_cancels:
        return
    all_hitboxes = set()
    for ac in auto_cancels:
        if ac.condition == 5:
            all_hitboxes = set(p.script.hit_hitboxes)
            for eff in p.effects or []:
                all_hitboxes.update(eff.script.hit_hitboxes)
        if ac.can_hit_transition(True, all_hitboxes, True):
            return ac
        if ac.condition == 15 and p.armor:
            return ac


def execute_opponent_transition(data: SFVData, p: PlayerState, script: MoveScript,
                                script_opponent: MoveScript) -> Tuple[MoveScript, MoveScript]:
    # select the opponent move pre-transition
    opponent_move = data.get_current_move_description(p.opponent)
    # Separated because it depends on the transition step
    others = script.get_others(p.script.time)
    if (0, 4) in others:
        # Tech throw
        if opponent_move is not None and (opponent_move.properties & 0x20):
            transition_logger.info("[%s][%s] Throw tech! Opponent move %s", p.char, p.script, opponent_move.name)
            # the opponent is/was doing a throw
            # p is the attacker
            p.select_scripts([40])
            # p.opponent is the defender
            p.opponent.select_scripts([41])
            script_opponent = data.get_current_script(p.opponent)
            p.pos = Position(p.opponent.pos.ref[0], p.opponent.pos.ref[1])
            p.opponent.script.init = False
            p.script.init = False
            # avoid going into (0,2)
            return data.get_current_script(p), script_opponent
        # TODO: Post hit throw tech, needs special cancel or input based interruption (like done in the game)

    if (0, 2) in others:
        other = others[(0, 2)]
        # Throws, Command Grabs, some CA
        # Opponent transition into character script
        # Params[0] = 1 -> Start
        # Params[0] = 0 -> End of the script
        script_id = other.params[1]
        if other.params[0]:
            transition_logger.info("[%s][%s] Transitioning opponent to script %d", p.char, p.script, script_id)
            p.opponent.select_scripts([script_id])
            p.opponent.script.time = other.params[2]
            p.opponent.script.temp_char = p.char
            p.opponent.script.init = False
            p.opponent.pos = Position(p.pos.ref[0], p.pos.ref[1])
            p.opponent.pos.side = p.pos.side
            p.opponent.stun = None
            p.opponent.stun_force = None
            p.opponent.juggle_state = "NONE"
            p.opponent.juggle = 0
            p.opponent.lock_transition = True
        else:
            transition_logger.info("[%s][%s] Disabling transition locking %d", p.char, p.script, script_id)
            p.opponent.lock_transition = False
        script_opponent = data.get_current_script(p.opponent)

    return script, script_opponent


def final_transition(p: PlayerState, script: MoveScript, go_to_next: Optional[AutoCancel]):
    """
    Transition step executed at the end of the frame

    :param p: the player state
    :param script: the script
    :param go_to_next: the optional auto cancel coming from earlier steps (landing for instance)
    """
    if p.script.countdown is not None:
        p.script.countdown -= 1

    # decrease vtrigger only if both are not in stun
    if p.stun is None and p.opponent.stun is None and p.vtrigger > 0:
        p.vtrigger -= 1

    if p.script.init:
        # We have changed script, therefore we cannot use the current script
        return

    auto_cancel_post_hit = get_auto_cancel_transition_post_hit(p, script)

    if auto_cancel_post_hit:
        go_to_next = auto_cancel_post_hit

    # apply time freeze (meaning stun for the opponent)
    others = script.get_others(p.script.time)
    time_freeze = others.get((1, 0))
    if time_freeze:
        freeze_frames = time_freeze.params[1] + 2
        p.opponent.stun = (p.opponent.stun or 0) + freeze_frames
        for e in p.opponent.effects + p.effects:
            e.stun = (e.stun or 0) + freeze_frames
    p.auto_cancel = go_to_next
