var movesData;

function toggleChangeInit(checkbox) {
    var target = $(checkbox).parents(".timeline").find(".change-init");
    if (checkbox.checked) {
        target.show();
    } else {
        target.hide();
        target.find(".script-init").val(0).trigger("change");
        target.find(".script-init-time").val(0);
        target.find(".vtrigger-checkbox input").prop("checked", false);
    }
}

function addYoutubeTutorial() {
    var tutorialDiv = $("#tutorial");
    if (tutorialDiv.html().indexOf("iframe") < 0) {
        var width = tutorialDiv.innerWidth();
        var height = width * 9 / 16;
        tutorialDiv.append('<iframe width="'+width+'" height="'+height+'" src="https://www.youtube.com/embed/adrEMvixODg?rel=0" frameborder="0" allowfullscreen></iframe>');
    }
}

function launchSimulation(button) {
    button.disabled = true;
    var previousButtonValue = $(button).html();
    $(button).html("In progress...");
    var body = {player_1: asScript(1), player_2: asScript(2), stop_condition: "BOTH"};
    localStorage.setItem("sim", JSON.stringify(body));
    var script = JSON.stringify(body);
    var urlHash = scriptsToUrl(body);
    $("#script-render").html(urlHash);
    $("#modal-script").val(urlHash);
    $("#modal-status").hide();
    $.ajax({
        url: "simulation",
        method: "POST",
        data: script,
        headers: {
            "Content-Type": "application/json"
        },
        success: function (data) {
            currentSimulation = data.frames;
            var maxCombo = [0, 0];
            data.frames.forEach(function(f) {
                if (f[0].combo_counter > maxCombo[0]) maxCombo[0] = f[0].combo_counter;
                if (f[1].combo_counter > maxCombo[1]) maxCombo[1] = f[1].combo_counter;
                f[0].max_combo = maxCombo[0];
                f[1].max_combo = maxCombo[1];
            });
            $(".shareable-url input").val(window.location.origin+window.location.pathname+"#"+urlHash);
            $(".shareable-url").show();
            $(".error-label").html(data.error || "");
            currentFrame = currentSimulation.length - 1;
            isStopped = true;
            $("#frame-slider").slider({
                value: currentFrame,
                min: 0,
                max: currentSimulation.length - 1,
                step: 1,
                slide: function (event, ui) {
                    currentFrame = ui.value;
                    pause();
                    drawScene(ui.value);
                }
            });
            drawScene(currentFrame);
            var lastFrame = data.frames[currentFrame];
            var centerX = (lastFrame[0].pos.coord[0] + lastFrame[1].pos.coord[0]) / 2;
            var scene = $(".scene-container").parent()[0];
            scene.scrollLeft = (scene.scrollWidth - scene.clientWidth) * (centerX / 8 + 0.75 - scene.clientWidth / scene.scrollWidth);
            $(".controls .time").show();
        },
        complete: function () {
            button.disabled = false;
            $(button).html(previousButtonValue);
            $("#play-button").focus();
        }
    });
}

function asScript(side) {
    var root = $("#panel-p" + side);
    var timeline = [];
    var previousItem = null;
    root.find(".timeline-item").each(function (i, e) {
        var move = $(e).find(".move")[0].getGroupAndValue();
        var item = [move[1]];
        var hold = $(e).find(".button-hold").val().trim();
        var delayInput = $(e).find(".move-delay");
        if (delayInput.is(":visible")) {
            var delay = delayInput.val().trim();
            previousItem[1] = parseInt(delay);
            previousItem[2] = parseInt(Math.max(move[0], 0));
        }
        timeline.push(item);
        item.push(null);
        item.push(null);
        item.push(parseInt(hold));
        previousItem = item;
    });
    return {
        char: root.find(".char").val(),
        init: {
            position: [parseFloat(root.find(".pos-x").val()), parseFloat(root.find(".pos-y").val())],
            script: parseInt(root.find(".script-init").val()),
            time: parseInt(root.find(".script-init-time").val()),
            vtrigger: 0 + root.find(".vtrigger-checkbox input").prop("checked")
        },
        resting_position: parseInt(root.find(".script-rest").val()),
        hit_effect: root.find(".hit-effect").val(),
        rise: root.find(".rise").val(),
        timeline: timeline
    };
}

function fromScript(side, script) {
    var root = $("#panel-p" + side);
    var selectChar = $("#char-p" + side);
    if (!movesData[script.char]) { throw new Exception("Missing character!"); }
    selectChar.val(script.char);
    selectCharacter(selectChar[0]);
    root.find(".script-init").val(script.init.script).trigger("change");
    root.find(".script-init-time").val(script.init.time || 0);
    root.find(".vtrigger-checkbox input").prop("checked", !!script.init.vtrigger);
    root.find(".move")[0].setGroupAndValue(script.timeline[0][0]);
    root.find(".timeline-item").find(".button-hold").val(script.timeline[0][3] || "1");
    var plusButton = root.find(".add-timeline-item")[0];
    for (var i = 1; i < script.timeline.length; i++) {
        var tl = script.timeline[i];
        var prTL = script.timeline[i-1];
        var move = tl[0];
        if (prTL.length >= 3 && prTL[2] !== null) {
            move = [prTL[2], move];
        }
        var select = addTimelineItem(plusButton);
        select[0].setGroupAndValue(move);
        select.closest(".timeline-item").find(".move-delay").val(prTL[1] || "0");
        select.closest(".timeline-item").find(".button-hold").val(tl[3] || "1");
    }
    root.find(".pos-x").val(script.init.position[0]);
    root.find(".pos-y").val(script.init.position[1]);
    root.find(".script-rest").val(script.resting_position);
    root.find(".hit-effect").val(script.hit_effect);
    root.find(".rise").val(script.rise);
    var changeInitCheckbox = root.find(".change-init-checkbox")[0];
    changeInitCheckbox.checked = !(script.init.time === 0 && script.init.script === 0) || script.init.vtrigger;
    toggleChangeInit(changeInitCheckbox);
}

function addTimelineItem(button, focus) {
    var selectedNode;
    var newNode = $('<div class="timeline-item col-xs-12"></div>');
    var timeline = $(button).closest(".timeline");
    newNode.html(timeline.data("timeline-item-html"));
    if ($(button).hasClass("panel-footer")) {
        selectedNode = timeline.find(".timeline-item").last();
    } else {
        selectedNode = $(button).closest(".timeline-item");
    }
    selectedNode.after(newNode);
    var select = $("select", newNode);
    initTimelineItem(select[0], selectedNode.find("select")[0]);
    timeline.sortable({items: ".timeline-item"});
    if (focus) {
        select.select2("open");
    }
    return select;
}

function removeTimelineItem(button) {
    var selectedParent = $(button).closest(".timeline-item");
    var timeline = $(button).closest(".timeline");
    selectedParent.remove();
    timeline.trigger("sortupdate");
}

function selectCharacter(select) {
    var suffix = select.id.indexOf("-p1") > 0 ? "-p1" : "-p2";
    var char = $(select).val();
    var data = movesData[char];
    $("#script-rest" + suffix).html(data.rest.map(function (script) {
        return "<option value=\"" + script[0] + "\">" + script[1] + "</option>";
    }).join(""));
    var selectScript = $("#script-init" + suffix);
    selectScript.html(data.scripts.map(function (script) {
        return "<option value=\"" + script[0] + "\">" + script[1] + "</option>";
    }).join(""));
    selectScript.select2().on("change", function() {
        initTimelineItem($("#panel" + suffix + " .move")[0], selectScript[0]);
    });

    var timeline = $("#panel" + suffix + " .timeline");

    // clean timeline
    var timelineItem = $("#panel" + suffix + " .timeline-item");
    timelineItem.each(function (i, dom) {
        if (i > 0) {
            dom.parentNode.removeChild(dom);
        }
    });
    timeline.data("char-data", data);
    if (!timeline.data("timeline-item-html")) {
        timeline.data("timeline-item-html", timelineItem.html());
    }

    // set moves
    var selectMove = $("#panel" + suffix + " .move");
    initTimelineItem(selectMove[0], selectScript[0]);
    timeline.sortable({
        items: ".timeline-item",
        placeholder: "ui-sortable-placeholder"
    }).on("sortupdate", function(event, ui) {
        initTimelineItem($("#panel" + suffix + " .move")[0], selectScript[0]);
    });
    selectMove.val("STAND").trigger("change");
}
var isInfiniteMove = {"STAND": true, "FORWARD": true, "CROUCH": true, "BACKWARD": true, "FORWARD_CROUCHED": true};
function initTimelineItem(select, previousSelect) {
    var prevSelection = [null, null];
    var charData = $(select).closest(".timeline").data("char-data");
    previousSelect = $(previousSelect);
    var cancels;
    if (previousSelect.hasClass("script-init")) {
        // this is a script
        var scriptId = parseInt(previousSelect.val());
        cancels = charData.scripts.filter(function (a) {
            return a[0] === scriptId;
        })[0][2];
    } else {
        // this is a move
        var moveName = previousSelect.val();
        cancels = charData.moves.filter(function (a) {
            return a[0] === moveName;
        })[0][1];
    }
    if (select.selectedIndex >= 0) {
        prevSelection = select.getGroupAndValue();
    }
    // Cancel == 1000 is hold so we remove it to avoid displaying it
    cancels = cancels.slice(0).filter(function(a) { return a !== 1000; });
    // cancel group -10 is a link
    if (cancels.indexOf(0) < 0) {
        cancels.push(-10);
    }
    // cancel group -1 is movement
    cancels.push(-1);
    var hasSelected = false;

    var foundPrevMove = false;
    $(select).html(cancels.map(function (cancelList) {
        var allMoves;
        var groupHeader;
        if (cancelList >= 0) {
            allMoves = charData.cancels.filter(function (a) {
                return a[0] === cancelList;
            });
            if (allMoves.length === 0) return "";
            allMoves = allMoves[0][1];
            var groupLabel = cancelList === 0 ? "Rest Move" : "Cancels #"+cancelList;
            groupHeader = "<optgroup label='"+groupLabel+"' group-id='"+cancelList+"'>";
        } else if (cancelList === -10) {
            allMoves = charData.cancels[0][1];
            groupHeader = "<optgroup label='Link' group-id='"+cancelList+"'>";
        } else {
            allMoves = charData.cancels[charData.cancels.length - 1][1];
            groupHeader = "<optgroup label='Movement' group-id='"+cancelList+"'>";
        }
        return groupHeader + allMoves.map(function (move) {
            var selected = "";
            if (cancelList === prevSelection[0] && move === prevSelection[1]) {
                selected = " selected='selected'";
                hasSelected = true;
            }
            if (move === prevSelection[1]) {
                foundPrevMove = true;
            }
            return "<option value=\"" + move + "\""+selected+">" + move + "</option>";
        }).join("")+"</optgroup>";
    }).join(""));

    var timelineItem = $(select).closest(".timeline-item");
    var timing = timelineItem.find(".timing");
    var hold = timelineItem.find(".hold");
    select.getGroupAndValue = function() {
        if (this.selectedIndex < 0) return [null, null];
        var option = this.options[this.selectedIndex];
        var group = parseInt($(option.parentNode).attr("group-id"));
        return [group, option.value];
    };
    select.setGroupAndValue = function(toSearch) {
        if (typeof toSearch !== "string" && toSearch[0] === 0) {
            toSearch = toSearch[1];
        }
        var pattern;
        if (typeof toSearch === "string") {
            pattern = "option[value='" + toSearch + "']";
        } else {
            pattern = "optgroup[group-id='" + toSearch[0] + "'] option[value='" + toSearch[1] + "']";
        }
        var search = $(pattern, $(select));
        if (search.length) {
            // always select the far down (if we wanted a cancel list, we would have asked for it)
            search[search.length-1].selected = true;
        } else {
            select.selectedIndex = 0;
        }
        $(select).trigger("change");
    };
    var onChange = function() {
        var isFirst = !timelineItem.prev().hasClass("timeline-item");
        var groupVal = select.getGroupAndValue();
        if (!isFirst) {
            if (groupVal[0] >= 0) {
                timing.show();
            } else if (groupVal[0] === -1 && isInfiniteMove[timelineItem.prev().find("select")[0].getGroupAndValue()[1]]) {
                timing.show();
            } else {
                timing.hide();
            }
        } else {
            timing.hide();
        }
        var cancels = $(select).closest(".timeline").data("char-data").moves.filter(function (a) {
            return a[0] === groupVal[1];
        })[0][1];
        if (cancels.indexOf(1000) >= 0) {
            if (!hold.find("input").val()) {
                hold.find("input").val(1);
            }
            hold.show();
        } else {
            hold.find("input").val("");
            hold.hide();
        }
        var nextSelection = timelineItem.next().find(".move");
        if (nextSelection.length > 0)
            initTimelineItem(nextSelection[0], select);
    };
    $(select).select2({
        templateSelection: function(selection) {
            var groupId = parseInt($(selection.element.parentNode).attr("group-id"));
            var label = "";
            if (groupId > 0) {
                label = "<strong>XX</strong> ";
            } else if (groupId === 0) {
                label = "";
            } else if (groupId === -1) {
                label = "";
            } else if (groupId === -10) {
                label = "<strong>&gt;</strong> ";
            }
            return $("<span>"+label+selection.text+"</span>");
        }
    }).on("change", onChange);
    // If group is not available anymore but we can
    if (!hasSelected && foundPrevMove) {
        $(select).val(prevSelection[1]).trigger("change");
    } else {
        onChange();
    }
}
var charLabels = {
    "ALX": "Alex",
    "BLR": "Vega [Claw]",
    "BRD": "Birdie",
    "BSN": "Balrog [Boxer]",
    "CMY": "Cammy",
    "CNL": "Chun-Li",
    "DSM": "Dhalsim",
    "FAN": "F.A.N.G.",
    "GUL": "Guile",
    "IBK": "Ibuki",
    "JRI": "Juri",
    "KEN": "Ken",
    "KRN": "Karin",
    "LAR": "Laura",
    "NCL": "Necalli",
    "NSH": "Nash",
    "RMK": "R. Mika",
    "RSD": "Rashid",
    "RYU": "Ryu",
    "URN": "Urien",
    "VEG": "M. Bison [Dictator]",
    "Z20": "Kolin",
    "Z21": "Akuma",
    "Z22": "Ed",
    "Z23": "Menat",
    "Z24": "Abigail",
    "Z25": "Zeku",
    "Z26": "Sakura",
    "Z27": "Blanka",
    "ZGF": "Zangief"
};
function initMoves(moves) {
    var characters = Object.keys(moves).sort();
    for (var i = 0; i < characters.length; i++) {
        var char = characters[i];
        var idx = moves[char].scriptIdx = {};
        moves[char].scripts.forEach(function (s) {
            idx[s[0]] = s[1];
        });
    }
    $("#char-p1").append($(characters.filter(function (char) {
        return char !== "CMN";
    }).map(function (char) {
        return "<option value=\"" + char + "\">"+char+" " + charLabels[char] + "</option>";
    }).join("")));
}

function cloneLeftPanel() {
    var content = $("#panel-p1").html();
    var contentP2 = content.replace(new RegExp("-p1", "g"), "-p2");
    $("#panel-p2").html(contentP2);
    $("#panel-p2 .player-title").html("Player 2");
    $("#pos-x-p2").val(1.5);
}

var captchaWidgetId;
function submitReport(token) {
    var $form = $("#report-modal").find("form");
    var button = $form.find("button.validate");
    button[0].disabled = true;
    button.html('<span class="glyphicon glyphicon-refresh glyphicon-refresh-animate spinning"></span> Sending...');
    $.ajax({
        url: "./report",
        type: "POST",
        data: $form.serialize(),
        success: function() {
            button.html('Thank you!');
            setTimeout(function() {
                $("#report-modal").modal("hide");
                $form.find("#report-message").val("");
                $form.find("#report-contact").val("");
            }, 2000);
        },
        error: function() {
            button.html("Failed! U BOT? ME BROKEN?");
            resetCaptcha();
        },
        complete: function() {
            $form.find("button.validate")[0].disabled = false;
            setTimeout(function() {
                button.html('Send Report');
            }, 2000);
        }
    });
}

function resetCaptcha() {
    if (typeof captchaWidgetId === 'undefined') {
        captchaWidgetId = grecaptcha.render($("#report-modal").find("button.validate")[0], {
            sitekey: "6LfeCiIUAAAAAJDMpBvU4bSbw5vIePma71uI0V53",
            badge: "inline",
            callback: submitReport,
            size: "invisible"
        });
    } else {
        grecaptcha.reset(captchaWidgetId);
    }
}
function charScriptToUrl(script) {
    var tl = JSON.stringify(script.timeline).replace(/null/g, '~');
    return [script.char, script.init.position[0], script.init.position[1], script.init.script, script.init.time,  script.init.vtrigger, script.resting_position, script.hit_effect, script.rise, tl].join("*");
}

function scriptsToUrl(script) {
    return encodeURIComponent(charScriptToUrl(script.player_1)+"!"+charScriptToUrl(script.player_2)+"!"+script.stop_condition);
}

function stringToCharScript(str) {
    var c = str.replace(/~/g, 'null').split("*");
    return {
        "char": c[0],
        "init": {"position": [parseFloat(c[1]), parseFloat(c[2])], "script": parseInt(c[3]), "time": parseInt(c[4]), "vtrigger": parseInt(c[5])},
        "resting_position": parseInt(c[6]),
        "hit_effect": c[7],
        "rise": c[8],
        "timeline": JSON.parse(c[9])
    };
}
function urlToScript(search) {
    search = decodeURIComponent(search).split("!");
    return {player_1: stringToCharScript(search[0]), player_2: stringToCharScript(search[1]), stop_condition: search[2]};
}

$(document).ready(function () {
    $(".legend").hide();
    initGrid();
    $.getJSON("moves", function (moves) {
        var storedScript;
        movesData = moves;
        $(".controls .time").hide();
        initMoves(moves);
        $(".debug-checkbox input")[0].checked = false;
        $(".debug").hide();
        cloneLeftPanel();
        if (location.hash) {
            storedScript = urlToScript(location.hash.substring(1));
            location.hash = "";
        } else {
            storedScript = localStorage.getItem("sim");
        }
        if (storedScript) {
            try {
                if (typeof storedScript === "string")
                    storedScript = JSON.parse(storedScript);
                fromScript(1, storedScript.player_1);
                fromScript(2, storedScript.player_2);
            } catch (e) {
                selectCharacter($("#char-p1")[0]);
                selectCharacter($("#char-p2")[0]);
                toggleChangeInit($("#change-init-p1")[0]);
                toggleChangeInit($("#change-init-p2")[0]);
            }
        } else {
            selectCharacter($("#char-p1")[0]);
            selectCharacter($("#char-p2")[0]);
            toggleChangeInit($("#change-init-p1")[0]);
            toggleChangeInit($("#change-init-p2")[0]);
        }
        var scene = $(".scene-container").parent()[0];
        scene.scrollLeft = (scene.scrollWidth - scene.clientWidth) / 2;
    });
    $('#report-modal').on('shown.bs.modal', function (e) {
        resetCaptcha();
    });

    $(window).on('hashchange', function() {
        if (location.hash) {
            var storedScript = urlToScript(location.hash.substring(1));
            location.hash = "";
            fromScript(1, storedScript.player_1);
            fromScript(2, storedScript.player_2);
        }
    });
    new Clipboard(".btn-clipboard");
});
