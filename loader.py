import gzip
import os
import pickle
import re

import argparse

import bson
import json

from model import *

parser = argparse.ArgumentParser()
parser.add_argument("--json", help="switch parsing to json instead of the default bson", action="store_true")
parser.add_argument("--version", help="version name", default=None)
parser.add_argument("--target", help="Target file to write (default data.bin or the version file.bin)", default=None)
parser.add_argument("pak_path", help="/path/to/Content/Paks/moves/StreetFighterV/Content/Chara")
args = parser.parse_args()

basepath = args.pak_path
target_version = args.version

if target_version is not None:
    target_version = re.sub("\\.txt$", "", target_version)
    print("Loading version "+target_version)
    to_load = dict()
    for line in open("versions/"+target_version+".txt"):
        char, v = line.split(' ')
        to_load[char] = v.strip()
    target = "versions/"+target_version+".bin" if args.target is None else args.target
else:
    print("Loading latest version")
    target = "data.bin" if args.target is None else args.target
    to_load = None

ignore = {"NDK", "AS1", "AS4", "AS6", "B00", "X00"}
chars = dict()

def bson_loads(path):
    return bson.loads(open(path, mode='rb').read())

def json_loads(path):
    json_str = open(path, encoding="utf-8", errors='ignore').read()
    json_str = re.sub(",[ \t\r\n]*}", "}", json_str)
    json_str = re.sub(",[ \t\r\n]*\]", "]", json_str)
    json_str = re.sub("}[ \t\r\n]*{", "},{", json_str)
    return json.loads(json_str)

source_load = json_loads if args.json else bson_loads

for char in (os.listdir(basepath) if to_load is None else to_load.keys()):
    if char in ignore:
        continue
    if not os.path.isdir(basepath + "/" + char):
        continue
    if to_load is None:
        versions = sorted([r for r in os.listdir(basepath + "/" + char + "/BattleResource") if re.match("[0-9]+", r) and r != "500"])
        if not versions:
            continue
        version = versions[-1]
    else:
        version = to_load[char]
    char_path = basepath + "/" + char + "/BattleResource/" + version
    try:
        BCH = source_load(char_path + "/BCH_" + char + ".json").get("BCH", dict())
        BCM = source_load(char_path + "/BCM_" + char + ".json")
        BAC = source_load(char_path + "/BAC_" + char + ".json")
    except Exception as e:
        print("Couldnt load all files for %s" % char, e)
        continue
    if os.path.exists(char_path+"/BAC_"+char+"_eff.json"):
        BAC_eff = source_load(char_path + "/BAC_" + char + "_eff.json")
        eff_scripts = [(MoveScript(char+"_eff", m) if m is not None else None) for m in BAC_eff["MoveLists"][0]["Moves"]]
        eff_hitbox_effects = [
            (
                {k: HitboxEffect(v) for k, v in m.items() if "_" in k and v is not None and "UNKNOWN" not in k}
                if m is not None else None
            )
            for m in BAC_eff["HitboxEffectses"]
        ]
    else:
        eff_scripts = []
        eff_hitbox_effects = []
    moves = [Move(m) for m in BCM["Moves"]]
    cancel_lists = [CancelList(cl) for cl in BCM["CancelLists"]]
    move_scripts = [(MoveScript(char, m) if m is not None else None) for m in BAC["MoveLists"][0]["Moves"]]
    if len(BAC["MoveLists"]) > 1:
        # print("   Alt moves detected")
        alt_move_scripts = [(MoveScript(char, m, True) if m is not None else None) for m in BAC["MoveLists"][1]["Moves"]]
        move_scripts = move_scripts + [None] * (2000-len(move_scripts)) + alt_move_scripts
    else:
        alt_move_scripts = None
    hitbox_effects = [
        (
            {k: HitboxEffect(v) for k, v in m.items() if "_" in k and v is not None}
            if m is not None else None
        )
        for m in BAC["HitboxEffectses"]
    ]
    chars[char] = CharacterData(
        move_scripts, hitbox_effects, cancel_lists, moves, eff_scripts, eff_hitbox_effects,
        BCH.get("VT1_Time", -1)
    )
    print(char, version)
    print("\tFound %d moves, %d cancel lists and %d scripts" % (len(moves), len(cancel_lists), len(move_scripts)))

with gzip.open(target, mode="wb") as f:
    pickle.dump(SFVData(chars), f)
