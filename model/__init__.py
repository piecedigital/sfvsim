from typing import Dict

from .cancel import *
from .hitbox_effect import *
from .move import *
from .move_script import *
from .simulator_state import *


class CharacterData:
    def __init__(
            self,
            scripts: List[Optional[MoveScript]],
            hitbox_effects: List[Optional[Dict[str, HitboxEffect]]],
            cancel_lists: List[CancelList],
            moves: List[Move],
            eff_scripts: List[Optional[MoveScript]],
            eff_hitbox_effects: List[Optional[Dict[str, HitboxEffect]]],
            vtrigger_time: int
    ):
        self.vtrigger_time = vtrigger_time
        self.moves = moves
        self.cancel_lists = cancel_lists
        self.hitbox_effects = hitbox_effects
        self.scripts = scripts
        self.eff_scripts = eff_scripts
        self.eff_hitbox_effects = eff_hitbox_effects

        if self.scripts[27] is not None and len(cancel_lists) >= 4:
            # walljump implementation: add it to the move pool
            m = Move(defaultdict(lambda: 0))
            m.index = len(moves)
            m.script = 27
            m.name = self.scripts[27].name
            m.position_restriction = 3
            # empirical value, never actually checked
            m.restriction_distance = FLT(1.4)
            moves.append(m)
            # add walljump to forward and backward jump cancel
            cancel_lists[2].moves.add(m.index)
            cancel_lists[3].moves.add(m.index)

        self.script_idx = {s.name: s.index for s in scripts if s is not None}
        self.move_idx = {m.name: (m.index, m.script) for m in moves if m is not None}


class SFVData:
    def __init__(self, chars: Dict[str, CharacterData]):
        self.chars = chars

    def get_current_script(self, p: PlayerState, script_id=None) -> MoveScript:
        char_data = self.chars[p.current_char()]
        if script_id is None:
            script_id = p.script.script
            if p.effects is None:
                return char_data.eff_scripts[script_id]
        s = None
        if p.script.stance != 0 and len(char_data.scripts) > script_id + 2000:
            s = char_data.scripts[script_id + 2000]
        if s is None:
            s = char_data.scripts[script_id]
        if s is None:
            return self.chars["CMN"].scripts[script_id]
        return s

    def get_hitbox_effect(self, char, effect_index, effect_type, position, is_effect=False) -> HitboxEffect:
        char_data = self.chars[char]
        table = char_data.eff_hitbox_effects[effect_index] if is_effect else char_data.hitbox_effects[effect_index]
        return table[effect_type + "_" + position]

    def get_cancels(self, char, script: MoveScript, tick) -> Set[Move]:
        if script is None:
            return set()
        cancels = set()
        cancel_lists = script.get_cancel_lists(tick)
        char_data = self.chars[char]
        for cl_id in cancel_lists:
            cl = char_data.cancel_lists[cl_id]
            for c in cl.moves:
                cancels.add(c)
        return cancels

    def get_scripts_by_move_name(self, char, move_name) -> Tuple[List[int], Optional[Move]]:
        scripts = get_reserved_scripts(char, move_name)
        if scripts is not None:
            return scripts, None
        move_description = self.chars[char].move_idx[move_name]
        return [move_description[1]], self.chars[char].moves[move_description[0]]

    def get_current_move_description(self, p: PlayerState):
        if p.move_id is None:
            return None
        return self.chars[p.char].moves[p.move_id]
