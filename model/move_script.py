from typing import List, Tuple, Optional, Set, Dict, Any

from collections import defaultdict

from .cancel import reserved_cancel_list
from .utils import *


class Timeline:
    def __init__(self, cls, data):
        self.empty = data is None or len(data) == 0
        if self.empty:
            return
        self.min = 100000
        self.max = -1
        if cls is not None:
            self.instances = [cls(d) for d in data]
        else:
            self.instances = data

        self.timeline = dict()
        self.init_timeline()

    def init_timeline(self):
        for d in self.instances:
            start = d.tick_start
            end = d.tick_end
            if start < self.min:
                self.min = start
            if end > self.max:
                self.max = end
            for frame in range(start, end):
                if frame not in self.timeline:
                    self.timeline[frame] = [d]
                else:
                    self.timeline[frame].append(d)

    def __getstate__(self):
        if self.empty:
            return []
        return self.instances

    def __setstate__(self, state):
        self.__init__(None, state)

    def at(self, tick) -> List:
        if not self.empty and not (tick < self.min or tick >= self.max) and tick in self.timeline:
            return self.timeline[tick]
        return []


class DictTimeline(Timeline):

    def init_timeline(self):
        for d in self.instances:
            start = d.tick_start
            end = d.tick_end
            if start < self.min:
                self.min = start
            if end > self.max:
                self.max = end
            for frame in range(start, end):
                if frame not in self.timeline:
                    self.timeline[frame] = dict()
                self.timeline[frame][d.type] = d

    def at(self, tick) -> Dict:
        if not self.empty and not (tick < self.min or tick >= self.max) and tick in self.timeline:
            return self.timeline[tick]
        return dict()


class TimeElement:
    def __init__(self, data):
        self.tick_end = data["TickEnd"]
        self.tick_start = data["TickStart"]

    def __str__(self):
        return type(self).__name__ + str(self.__dict__)

    def copy(self, data):
        self.tick_end = data.tick_end
        self.tick_start = data.tick_start


class AutoCancel(TimeElement):
    def __init__(self, data):
        super().__init__(data)
        self.move_index = data["MoveIndex"]
        self.time = data["Unknown1"]
        self.transition = data["Unknown2"]
        """
        Conditions:
            0:  Always
            1:  On Hit
            2:  On Block
            3:  TODO Whiff
            4:  Pre Hit
            5:  Post Hit
            6:  NA
            7:  NA
            8:  Reached Ground
            9:  Camera Jump
            10: NA
            11: Useless ??
            12: Close to the opponent
            13: NA
            14: TODO Button release
            15: TODO If hit ??
            16: TODO If hit ??
            17: TODO Armored
            18: NA
            19: NA
            20: TODO Has resources (param 2, 1 == no kunais, 2 == is vtrigger) 
            21: TODO CA Transition (not executed?)
            22: NA
            23: Useless ??
            24: TODO Button Held
            25: NA
            26: TODO Contains projectile 
        """
        self.condition = data["Condition"]
        # temporary support for MM
        if self.condition == 0:
            self.condition = "Always"
        elif self.condition == 2:
            self.condition = "OnBlock"
        self.params = data.get("Ints")

    def can_hit_transition(self, has_hit, hit_hitboxes, post_hit=False):
        base_condition = 1 if has_hit else "OnBlock"
        if self.condition == base_condition and not post_hit:
            return True
        if self.condition == 4 and not post_hit or self.condition == 5 and post_hit:
            hitbox = 0 if not self.params else self.params[0]
            mode = 0 if not self.params else self.params[3]
            if mode == 0:
                return hitbox in hit_hitboxes
            else:
                return len(hit_hitboxes) >= hitbox
        return False


def AltAutoCancel(data):
    if data["MoveIndex"] >= 0:
        data["MoveIndex"] += 2000
    return AutoCancel(data)


class Cancel(TimeElement):
    def __init__(self, data):
        super().__init__(data)
        self.cancel_list = data["CancelList"]
        self.type = data["Type"]


class Box(TimeElement):
    def __init__(self, data):
        super().__init__(data)
        self.type = -1
        self.x = FLT(data["X"])
        self.y = FLT(data["Y"])
        self.width = FLT(data["Width"])
        self.height = FLT(data["Height"])


class Hitbox(Box):
    def __init__(self, data):
        super().__init__(data)
        self.type = data["HitType"]
        self.effect_index = data["HitboxEffectIndex"]
        self.juggle_limit = data["JuggleLimit"]
        self.juggle_increase = data["JuggleIncrease"]
        self.flag = data["Unknown6"]
        self.box_id = data.get("Unknown8", 0)
        self.knockdown = data["Unknown7"]
        self.hit_range = data["Unknown10"]
        self.flag_hit = data["Unknown11"]
        self.number_of_hits = data["NumberOfHits"] & 255
        self.hit_level = data["NumberOfHits"] >> 8

    def get_id(self):
        return self.box_id & 255

    def reset_box(self):
        return self.get_id() == 255

    def get_priority(self):
        return self.box_id >> 8

    def cant_hurt_crouched(self):
        return self.flag & 2

    def cant_hurt_air(self):
        return self.flag & 4

    def cant_hurt_stand(self):
        return self.flag & 1


class Hurtbox(Box):
    def __init__(self, data):
        super().__init__(data)
        self.type = data["Flag1"]
        self.is_armor = data["HitEffect"] > 0
        self.hurt_type = data["Unknown10"]
        self.armor_count = data["Unknown9"]
        self.armor_group = data["Unknown8"]
        self.armor_freeze = data["Unknown11"]
        self.air_inv = data["Unknown6"] & 4 == 4

    def get_armor_group(self):
        return self.armor_group & 255

    def get_armor_freeze(self):
        return self.armor_freeze & 255

    def get_armor_count(self):
        return self.armor_count & 255

    def get_armor_priority(self):
        return self.armor_group >> 8

    def get_collision_priority(self):
        return -self.is_armor, self.hurt_type != 0, -self.hurt_type


class PositionShifts:
    X = 0x08000
    Y = 0x10000
    RELATIVE = 0x40000
    BOUND = 0x8
    HITBOX_SHIFT = 0x4

    def __init__(self, timeline, max_frames):
        lines = defaultdict(lambda: np.full(shape=(max_frames,), fill_value=np.nan, dtype=FLT))
        for data in timeline or []:
            if "Flag" not in data:
                continue
            flag = data["Flag"]
            line = lines[flag]
            value = data["Movement"]
            np.nan_to_num(line[data["TickStart"]:data["TickEnd"]], copy=False)
            line[data["TickStart"]:data["TickEnd"]] += value
        self.lines = dict()
        for flag, line in lines.items():
            nnz = np.nonzero(np.isfinite(line))[0]
            if nnz.shape[0] == 0:
                continue
            self.lines[flag] = (nnz[0], line[nnz[0]: (nnz[-1]+1)])

    def get(self, flag, time, default=None):
        if flag not in self.lines:
            return default
        t, line = self.lines[flag]
        if time < t:
            return default
        if time >= (line.shape[0]+t):
            return default
        value = line[time - t]
        return default if np.isnan(value) else value


class Force(TimeElement):
    def __init__(self, data):
        super().__init__(data)
        self.amount = data["Amount"]
        flag = data["Flag"]
        if flag == "HorizontalSpeed":
            flag = 0x1
        elif flag == "VerticalSpeed":
            flag = 0x10
        elif flag == "HorizontalAcceleration":
            flag = 0x1000
        elif flag == "VerticalAcceleration":
            flag = 0x10000
        if flag == 0:
            self.type = 0
            self.flag = 0
        elif flag & 0xf:
            self.type = 1
            self.flag = flag
        elif flag & 0xf0:
            self.type = 2
            self.flag = flag >> 4
        elif flag & 0xf00:
            self.type = 3
            self.flag = flag >> 8
        elif flag & 0xf000:
            self.type = 4
            self.flag = flag >> 12
        elif flag & 0xf0000:
            self.type = 5
            self.flag = flag >> 16
        else:
            print("Unknown flag???", flag)
            self.type = 6
            self.flag = flag >> 20

    def is_temporary(self):
        return self.flag & 8

    def is_on_speed_change(self):
        return self.flag & 4

    def is_self_multiply(self):
        return self.flag & 2

    def is_active(self):
        return self.flag & 1

    def is_one_frame(self):
        return (self.tick_end - self.tick_start) <= 1


class Type1(TimeElement):
    def __init__(self, data):
        super().__init__(data)
        self.flag = data["Flag1"]

    def is_crouched(self):
        return self.flag & 2 == 2

    def is_air(self):
        return self.flag & 4 == 4

    def is_counter(self):
        return self.flag & 32 == 32

    def is_on_ground(self):
        return self.flag & 32768 == 32768

    def is_auto_correct(self):
        return self.flag & 65536 == 65536

    def is_reverse_auto_correct(self):
        return self.flag & 131072 == 131072


class Other(TimeElement):
    def __init__(self, data):
        super().__init__(data)
        self.type = data["Unknown1"], data["Unknown2"]
        self.params = data["Ints"]


def AltOther(data):
    result = Other(data)
    if result.type == (0, 2):
        result.params[1] += 2000
    if result.type == (0, 7):
        result.params[0] += 2000
    return result


class MoveScript:
    def __init__(self, char, data, is_alternate=False):
        self.char = char
        self.index = data["Index"]
        if is_alternate:
            self.index += 2000
        self.name = data["Name"]
        self.flag = data["Flag"]
        self.slide = data["Slide"]
        self.slide_y = data["unk3"]
        self.slide_acc = data["unk5"]
        self.slide_y_acc = data["unk6"]
        self.last_hitbox_frame = data["LastHitboxFrame"]
        self.hurt_type = data["unk9"]
        self.total_ticks = data["TotalTicks"]
        self.interrupt_frame = data["InterruptFrame"]
        self.return_to_original_position = data["ReturnToOriginalPosition"]
        self.auto_cancels = Timeline(AltAutoCancel if is_alternate else AutoCancel, data["AutoCancels"])
        self.cancels = Timeline(Cancel, data["Cancels"])
        self.states = Timeline(Type1, data.get("Type1s", []))
        self.hitboxes = Timeline(Hitbox, data["Hitboxes"])
        self.hurtboxes = Timeline(Hurtbox, data["Hurtboxes"])
        self.physics_boxes = Timeline(Box, data["PhysicsBoxes"])
        self.position_shifts = PositionShifts(data["Positions"], self.total_ticks)
        self.forces = Timeline(Force, data["Forces"])
        self.others = DictTimeline(AltOther if is_alternate else Other, data["Others"])

    @cached_property
    def latest_hitbox(self):
        if self.hitboxes.empty:
            return None
        all_hitboxes_tick_end = [h.tick_start for h in self.hitboxes.instances if h.type != 4]
        return max(all_hitboxes_tick_end) if all_hitboxes_tick_end else None

    def is_bound(self):
        return self.flag == 196612

    def is_falling(self):
        return self.flag & 3 == 2 and self.flag > 2 or self.flag == 262148

    def is_landing_required(self):
        return self.flag & 0x20000

    def is_knockdown(self):
        return self.flag & 0x40000

    def is_on_ground(self):
        return self.flag & 2 == 0

    def get_hitboxes(self, tick) -> List[Hitbox]:
        return [build_copy(b, Hitbox) for b in self.hitboxes.at(tick)]

    def get_hurtboxes(self, tick) -> List[Hurtbox]:
        return [build_copy(b, Hurtbox) for b in self.hurtboxes.at(tick)]

    def get_physics_boxes(self, tick) -> List[Box]:
        return [build_copy(b, Box) for b in self.physics_boxes.at(tick)]

    def get_states(self, tick) -> List[Type1]:
        return self.states.at(tick)

    def get_auto_cancels(self, tick) -> List[AutoCancel]:
        return self.auto_cancels.at(tick)

    def get_cancels(self, tick) -> List[Cancel]:
        return self.cancels.at(tick)

    def get_cancel_buffers(self, tick) -> Dict[int, int]:
        cl = {
            cancel.cancel_list: cancel.type
            for cancel in self.get_cancels(tick)
            if cancel.cancel_list >= 0 and cancel.type < 8
        }
        for rcl in reserved_cancel_list(self.char, self.index):
            cl[rcl] = 0
        return cl

    def get_cancel_lists(self, tick) -> Set[int]:
        cl = {cancel.cancel_list for cancel in self.get_cancels(tick) if cancel.cancel_list >= 0 and cancel.type == 8}
        return cl | reserved_cancel_list(self.char, self.index)

    def get_others(self, tick) -> Dict[Any, Other]:
        return self.others.at(tick)

    def get_forces(self, tick) -> List[Force]:
        return self.forces.at(tick)

    def get_script_length(self):
        return self.total_ticks if self.interrupt_frame < 0 else min(self.interrupt_frame+1, self.total_ticks)
