from typing import Union
from .utils import *


class Position:
    """
    @type shift: np.ndarray
    @type coord: np.ndarray
    @type force: np.ndarray
    @type ref: np.ndarray
    @type side: bool
    """
    def __init__(self, x: Union[float, FLT], y: Union[float, FLT]):
        self.coord = [FLT(x), FLT(y)]
        self.speed = [ZERO, ZERO]
        self.temp_speed = [None, None]
        self.on_speed_change = [None, None, None, None, None, None]
        self.force = [ZERO, ZERO]
        self.temp_force = [None, None]
        self.shift = [ZERO, ZERO]
        self.ref = [FLT(x), FLT(y)]
        self.side = True

    def __str__(self):
        # noinspection PyStringFormat
        return "(%s, shift=%s, ref=%s, speed=%s, force=%s)" % (self.coord, self.shift, self.ref, self.speed, self.force)


class StunForce:
    """
    @type shift: np.ndarray
    @type coord: np.ndarray
    @type force: np.ndarray
    @type ref: np.ndarray
    @type side: bool
    """
    def __init__(self):
        self.speed = [ZERO, ZERO]
        self.force = [ZERO, ZERO]
        self.t = None

    def __str__(self):
        # noinspection PyStringFormat
        return "(speed=%s, force=%s, t=%s)" % (self.speed, self.force, self.t)


class Script:
    def __init__(self):
        self.used_hitboxes = set()
        self.hit_hitboxes = set()
        self.script = 0
        self.time = 0
        self.previous_time = 0
        self.has_hit = 0
        self.hit_status = 0
        self.stance = 0
        self.temp_char = None
        self.countdown = None
        self.init = True
        self.used_script = None
        self.speed = None
        self.tick = 0

    def __str__(self):
        if self.tick == 0 and self.speed is None and self.init is False:
            return "script(%d, %d)" % (self.script, self.time)
        return "script(%d, %d, %d, %s, %s)" % (self.script, self.time, self.tick, self.speed, self.init)


class PlayerState:
    """
    @type stun_force: StunForce
    @type hit: list[model.Hitbox]
    @type hit_queue: list[tuple[model.Hitbox, model.Hurtbox]]
    @type armor: dict[int, list[model.Hitbox]]
    @type hurt: list[model.Hurtbox]
    @type phys: list[model.Box]
    @type opponent: PlayerState
    @type script: Script
    @type queue: list[int]
    @type effects: list[PlayerState]
    @type buffered_cancels: dict[int, int]
    """
    def __init__(self, char, x, y, opponent=None):
        self.corner_push_opponent = False
        self.pos = Position(x, y)
        self.char = char
        self.move_id = None
        self.hit = []
        self.hurt = []
        self.phys = []
        self.queue = []
        self.hit_queue = []
        self.armor = dict()
        self.script = None
        self.stance = 0
        self.stun_force = None
        self.juggle = 0
        self.hold = 0
        self.juggle_state = "NONE"
        self.stun = None
        self.recovery = 0
        self.hit_effect = "HIT"
        self.rise = "NONE"
        self.opponent = opponent
        self.downtime = 0
        self.combo_counter = 0
        self.status_air = False
        self.status_crouched = False
        self.status_counter = False
        self.disable_positions = False
        self.lock_transition = False
        self.knockdown_type = None
        self.auto_cancel = None
        self.buffered_cancels = dict()
        self.vtrigger = 0
        self.effects = []
        if opponent is not None:
            opponent.opponent = self
            self.select_scripts([0])
            opponent.select_scripts([0])
            self.pos.side = self.pos.coord[0] < opponent.pos.coord[0]
            opponent.pos.side = opponent.pos.coord[0] < self.pos.coord[0]

    def current_char(self):
        return self.char if self.script is None or self.script.temp_char is None else self.script.temp_char

    def reset(self, x, y):
        self.__init__(self.char, x, y, opponent=self.opponent)

    def change_script(self, script, time, reset_position_shift):
        self.script.init = script != self.script.script
        self.script.script = script
        self.script.active_hitboxes = set()
        self.script.used_hitboxes = set()
        self.script.hit_status = 0
        self.script.time = time
        self.script.tick = 0
        self.reset_script(reset_position_shift)

    def reset_script(self, reset_position_shift):
        if self.script.temp_char is not None:
            self.pos.side = self.pos.coord[0] < self.opponent.pos.coord[0]
            self.script.temp_char = None
        self.disable_positions = False
        self.lock_transition = False
        self.buffered_cancels = dict()
        if reset_position_shift:
            self.reset_shift()

    def hit_status(self, last_hitbox_frame) -> int:
        status = self.script.hit_status
        if status > 0:
            return status
        if last_hitbox_frame <= self.script.time:
            return 4
        if self.effects:
            for eff in self.effects:
                eff_status = eff.script.hit_status
                if 1 <= eff_status < 4:
                    return eff_status
        return 0

    def reset_shift(self):
        self.pos.ref[0] = self.pos.coord[0]
        self.pos.ref[1] = self.pos.coord[1]
        self.pos.shift = [ZERO, ZERO]

    def select_scripts(self, scripts):
        self.queue = scripts[1:]
        self.script = Script()
        self.script.script = scripts[0]
        self.script.stance = self.stance
        self.armor.clear()
        self.reset_script(True)
