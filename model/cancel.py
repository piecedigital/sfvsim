from collections import OrderedDict
from typing import Set, Iterable

REST_POSITIONS = {0, 1, 13, 14, 15}

STANDARD_MOVES = OrderedDict([
    ("STAND", [0] * 10), ("CROUCH", [1] * 10), ("FORWARD", [16] * 10), ("BACKWARD", [17] * 10),
    ("JUMP_V", [4, 5]), ("JUMP_F", [7, 8]), ("JUMP_B", [10, 11]), ("DASH", [18]),
    ("BACKDASH", [19])
])

NON_STANDARD_MOVE = {
    "GUL": OrderedDict({"FORWARD_CROUCHED": [20] * 10})
}

NON_STANDARD_REST = {
    ("CMY", 21), ("VEG", 20), ("Z22", 21)
}

non_standard_move_set = {
    (char, script_id[0])
    for char, actions in NON_STANDARD_MOVE.items()
    for script_id in actions.values()
}


def get_reserved_scripts(char, move_name):
    scripts = STANDARD_MOVES.get(move_name)
    if not scripts:
        scripts = NON_STANDARD_MOVE.get(char, dict()).get(move_name)
    return scripts


def is_resting_script(char, script_id):
    script_id %= 2000
    return script_id in REST_POSITIONS or (char, script_id) in NON_STANDARD_REST


def is_walking_script(char, script_id):
    return script_id in {16, 17} or (char, script_id) in non_standard_move_set


def reserved_cancel_list(char, index):
    index %= 2000
    if is_resting_script(char, index) or is_walking_script(char, index):
        return {0}
    elif index == 5:
        return {1}
    elif index == 8:
        return {2}
    elif index == 11:
        return {3}
    return set()


def get_recursive_cancels(data, script, visited_nodes: Set[int] = None) -> Set[int]:
    if visited_nodes is None:
        visited_nodes = set()
    visited_nodes.add(script.index)
    self_cancel = {cancel.cancel_list for cancel in script.cancels.instances if
                   cancel.cancel_list >= 0 and cancel.type == 8} if not script.cancels.empty else set()
    self_cancel.update(reserved_cancel_list(script.char, script.index))
    if not script.auto_cancels.empty:
        auto_cancels = {
            auto for auto in script.auto_cancels.instances
            if auto.move_index not in visited_nodes and
               auto.move_index >= 0 and not is_resting_script(script.char, auto.move_index)
        }

        if {auto for auto in auto_cancels if auto.condition in {14, 24}}:
            # can hold button cancel
            self_cancel.add(1000)
        for auto in auto_cancels:
            self_cancel.update(get_recursive_cancels(data, data.scripts[auto.move_index], visited_nodes))
    return self_cancel


def standard_move_cancels(char_data, scripts: Iterable[int]) -> Set[int]:
    result = set()
    for script_id in scripts:
        script = char_data.scripts[script_id]
        if script is not None:
            result.update(get_recursive_cancels(char_data, script))
    return result
