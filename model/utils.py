from typing import TypeVar
import numpy as np

FLT = np.float32
ZERO = FLT(0)
TWO = FLT(2)

T = TypeVar('T')


def build_copy(instance, clazz):
    """
    Make a copy of the object as fast as possible (not deep, only shallow)

    :param instance: the object instance
    :param clazz: the target class
    :return: a new instance on which all attributes are the same as the input instance
    """
    new_instance = object.__new__(clazz)
    new_instance.__dict__ = instance.__dict__.copy()
    return new_instance


# noinspection PyPep8Naming
class cached_property(object):
    """
    Source: https://github.com/pydanny/cached-property/blob/master/cached_property.py
    """

    def __init__(self, func):
        self.__doc__ = getattr(func, '__doc__')
        self.func = func

    def __get__(self, obj, cls):
        if obj is None:
            return self
        value = obj.__dict__[self.func.__name__] = self.func(obj)
        return value
