
class Move:
    def __init__(self, data):
        self.index = data["Index"]
        self.position_restriction = data["PositionRestriction"]
        self.restriction_distance = data["RestrictionDistance"]
        self.name = data["Name"]
        self.script = data["ScriptIndex"]
        self.type = (data["Unknown6"] & 0xff).bit_length()
        self.properties = data["Unknown6"] >> 8
        self.vtrigger_usage = data["Unknown16"] >> 16
        if data["Unknown8"] == 2:
            # It's an alternate script in the second move list
            self.script += 2000

    def get_height_restriction(self):
        if self.position_restriction == 3:
            return self.restriction_distance
        return None

    def is_wall_jump(self):
        return self.script == 27

    def is_vtrigger_activation(self):
        return self.type == 8


class CancelList:
    """
    @type moves: list[int]
    @type index: int
    """
    def __init__(self, data):
        self.index = data["Index"]
        self.moves = {c["Index"] for c in data["Cancels"] or []}

