class HitboxEffect:
    def __init__(self, data):
        self.type = data["Type"]
        self.index = data["Index"]
        self.damage_type = data["DamageType"]
        self.damage = data["Damage"]
        self.stun = data["Stun"]
        self.ex_attacker = data["EXBuildAttacker"]
        self.ex_defender = data["EXBuildDefender"]
        self.hitstun_attacker = data["HitStunFramesAttacker"]
        self.hitstun_defender = data["HitStunFramesDefender"]
        self.recovery_frames = data["RecoveryAnimationFramesDefender"]
        self.knock_back = data["KnockBack"]
        self.fall_speed = data["FallSpeed"]
        self.downtime = data.get("Index17", 0)
        self.juggle_start = data["Index18"] & 0xFF

    def is_crush(self):
        return self.damage_type & 32 > 0

    def is_hard_kd(self):
        return self.damage_type & 8 > 0

    def is_specific_script(self):
        return self.damage_type & 1 > 0
