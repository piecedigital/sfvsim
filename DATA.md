# Data Loading
The core data of SFVSim is all contained in one single file: `data.bin`. This is built based on what is inside SFV.
This page describes what is inside this file and how you can update the data from the game directory. This file is not
versioned in the project for obvious reasons (binary versioning yada yada).

## Just give me the file
If you don't want to build your own `data.bin`, I host versions of it [on my server](https://petitl.fr/sfv/)

## What's inside it
The file is a python pickle dump of the `model.SFVData` object. In order to have a lighter version, it is passed to a 
gzip compressor. This gives us a binary <3MB which is decent. LZMA could be used but it made the decompression process
too long.

The SFV Data entry contains a binary version of the "scripts". You can view those scripts using 
[TOOLASSISTED DIFF tool](https://toolassisted.github.io/DIFF/). Nothing more is used.

## Loading data from the game
As you may know, SFV data is located inside Pak files in `Content\Paks\` inside Steam `StreetFighterV` directory.

To extract the paks and convert the uassets files, you can download the [extraction toolkit](https://petitl.fr/sfv/extract-moves.zip)
I prepared. It is based on [quickbms](http://aluigi.altervista.org/quickbms.htm) and a modified version of the 
monstrously awesome [MoveTool](https://github.com/lullius/MoveTool) (I output BSON instead of JSON and I'm tolerant to
empty files).
  
Once you have downloaded the zip, extract everything in the `Content\Paks` directory and execute `extract-moves.bat`.
This will create a `moves` directory with all the `uassets` and the `json` files (that will be read be SFVSim).

Once you did that, you have to execute the loader script using
```
python loader.py /path/to/Content/Paks/moves/StreetFighterV/Content/Chara
```

This will take the latest version of each character available and produce `data.bin`.

To load a previous version, use the file name available in the [`versions`](versions)
directory. For instance, the following line will load data equivalent to the end of Season 1 as played in CPT in the
versions/s1-cpt-version.bin file:
```
python loader.py /path/to/Content/Paks/moves/StreetFighterV/Content/Chara s1-cpt-version
```
