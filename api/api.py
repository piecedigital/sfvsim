import hashlib
import json
import os
import requests
import time

import io

import sys
from bottle import post, get, response, request

from model import STANDARD_MOVES, NON_STANDARD_MOVE
from model.cancel import standard_move_cancels, get_recursive_cancels, is_resting_script
from simulation.move_queue import MoveQueue, execute_simulation, MoveQueueItem, CouldNotCancel
from simulation.simulator import Simulator, PlayerState, MoveNotAllowed
from api.util import json_request, api_logger, serialize_objects
from simulation.transitions import InfiniteTransitionException

if len(sys.argv) != 3:
    print("Usage: python server.py data.bin port")
    exit(1)
sim = Simulator(sys.argv[1])
cache = {}
port = int(sys.argv[2])

if os.path.exists("./config.json"):
    config = json.load(open("./config.json"))
else:
    config = dict()


def build_player(player_data):
    char = player_data["char"]
    position = player_data["init"]["position"]
    p = PlayerState(char, position[0], position[1])
    p.hit_effect = player_data["hit_effect"]
    p.rise = player_data["rise"]
    return p


def build_move_queue_item(item):
    if len(item) > 3:
        return MoveQueueItem(item[0], item[1] or 0, item[2], item[3])
    if len(item) > 2:
        return MoveQueueItem(item[0], item[1] or 0, item[2])
    if len(item) > 1:
        return MoveQueueItem(item[0], item[1])
    return MoveQueueItem(item[0])


def build_time_line(player_data):
    queue = [build_move_queue_item(item) for item in player_data["timeline"]]
    return MoveQueue(queue, player_data["resting_position"])


def init_player(p: PlayerState, p_opponent: PlayerState, init):
    p.opponent = p_opponent
    p.select_scripts([init.get("script", 0)])
    p.script.time = init.get("time", 0)
    p.vtrigger = sim.data.chars[p.char].vtrigger_time if init.get("vtrigger", False) else 0
    if p.vtrigger != 0 and p.char == "NCL":
        # Special case for NCL, vtrigger means stance change
        p.stance = 1
    p.pos.side = p.pos.coord[0] < p_opponent.pos.coord[0]


@get('/moves')
def moves():
    response.content_type = 'application/json'
    if "moves" in cache:
        return cache["moves"]
    moves = {char: {
        "rest": [
            [s.index, s.name]
            for s in char_data.scripts[:50] if s is not None and is_resting_script(char, s.index)
        ],
        "moves":
            [
                [move_name, standard_move_cancels(char_data, scripts)]
                for move_name, scripts in list(STANDARD_MOVES.items()) + list(NON_STANDARD_MOVE.get(char, {}).items())
            ] + [
                [m.name, get_recursive_cancels(char_data, char_data.scripts[m.script]) | (get_recursive_cancels(char_data, char_data.scripts[2000+m.script]) if len(char_data.scripts) >= (m.script+1999) and char_data.scripts[m.script+2000] is not None else set())]
                for m in char_data.moves
                if m is not None and char_data.scripts[m.script] is not None
            ],
        "scripts": [
            [s.index, s.name, get_recursive_cancels(char_data, s)]
            for s in char_data.scripts if s is not None
        ],
        "cancels":
            [
                [cl.index, [char_data.moves[move_id].name for move_id in cl.moves]]
                for cl in char_data.cancel_lists
                if cl is not None and cl.moves
            ] + [
                [-1, list(STANDARD_MOVES.keys()) + list(NON_STANDARD_MOVE.get(char, {}).keys())]
            ]}
        for char, char_data in sim.data.chars.items()
    }
    cache["moves"] = json.dumps(moves, default=serialize_objects)
    return cache["moves"]


@post('/report')
def send_report():
    if "gitlab-token" not in config:
        raise Exception("No gitlab key configured")
    captcha_response = request.forms.get('g-recaptcha-response')
    # CAPTCHA CHECK
    captcha = requests.post("https://www.google.com/recaptcha/api/siteverify", data={
        "secret": config.get("recaptcha-token"),
        "response": captcha_response
    }).json()
    if not captcha.get("success"):
        raise Exception("Could not validate spam verification %s", captcha.get("error-codes"))

    message = request.forms.get('message')
    contact = request.forms.get('contact')
    script = request.forms.get('script', '')
    script_hash = hashlib.sha1(script.encode()).hexdigest()[:8]
    issue = """Message: %s

Contact: `%s`

Script:
```
%s
```
""" % (message, contact, script)

    private_key = config["gitlab-token"]
    return requests.post("https://gitlab.com/api/v4/projects/3151794/issues", data={
        "title": "User report %s" % script_hash,
        "description": issue,
        "confidential": True
    }, headers={
        "PRIVATE-TOKEN": private_key
    }).json()


@post('/simulation')
@json_request
def simulation(body):
    """
    {
        "player_1": {
            "char": "CMY",
            "init": {
                "position": [-1.5, 0],
                "script": 0,
                "time": 0
            },
            "timeline": [
                ["FORWARD", 30],
                ["2HP", 24]
            ],
            "resting_position": 0,
            "hit_effect": "HIT"
        },
        "player_2": {},
        "stop_condition": "BOTH"
    }
    :param body: 
    :return: 
    """
    t = time.time()
    response.content_type = 'application/json'
    body_response = io.StringIO()
    body_response.write('{"frames": [')
    first = [True]

    def collect(p1, p2):
        p1.opponent = None
        p2.opponent = None
        for e in p1.effects:
            e.opponent = None
        for e in p2.effects:
            e.opponent = None
        if first[0]:
            first[0] = False
        else:
            body_response.write(",")
        body_response.write(json.dumps([p1, p2], default=serialize_objects))
        for e in p1.effects:
            e.opponent = p2
        for e in p2.effects:
            e.opponent = p1
        p1.opponent = p2
        p2.opponent = p1

    p1 = build_player(body["player_1"])
    p2 = build_player(body["player_2"])
    init_player(p1, p2, body["player_1"]["init"])
    init_player(p2, p1, body["player_2"]["init"])

    try:
        execute_simulation(sim, p1, build_time_line(body["player_1"]), p2, build_time_line(body["player_2"]), collect)
    except (MoveNotAllowed, CouldNotCancel, InfiniteTransitionException) as e:
        t = time.time() - t
        api_logger.info("Completed simulation in %.02f seconds with exception %s" % (t, str(e)))
        body_response.write('], "error":')
        body_response.write(json.dumps(str(e)))
        body_response.write('}')
        body_response.seek(0)
        return body_response.read()

    t = time.time() - t
    api_logger.info("Completed simulation in %.02f seconds" % t)
    body_response.write(']}')
    body_response.seek(0)
    return body_response.read()
