import datetime
import json

import logging

import numpy
from bottle import response, request

api_logger = logging.getLogger("api")


def json_body(f):
    def wrapper(*args, **kwargs):
        response.content_type = 'application/json'
        return json.dumps(f(*args, **kwargs), default=serialize_objects)
    return wrapper


def json_request(f):
    def wrapper(*args, **kwargs):
        body = request.json

        if body is None or not isinstance(body, dict):
            response.status = 400
            return {"error": "unreadable json body (requires the content-type header and a json object as body)"}

        return f(body=body, *args, **kwargs)
    return wrapper


def serialize_objects(a):
    if hasattr(a, "__dict__"):
        return a.__dict__
    if isinstance(a, set):
        return list(a)
    if isinstance(a, datetime.datetime):
        return datetime_to_jsontime(a)
    if isinstance(a, datetime.date):
        return a.isoformat()
    if isinstance(a, numpy.float32):
        return float(a)
    if isinstance(a, numpy.bool_):
        return bool(a)
    raise Exception("Cant serialize object %s" % str(a))


def json_datetime(jt):
    if jt is None:
        return None
    return datetime.datetime.fromtimestamp(jt/1000, tz=datetime.timezone.utc)


def datetime_to_jsontime(dt):
    if dt is None:
        return None
    return int(dt.timestamp() * 1000)