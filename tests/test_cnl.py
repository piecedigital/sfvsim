from tests.util import SimulationRun


def test_cnl_vt_5mp():
    run = SimulationRun("2.021", "CNL", "RYU", "cnl-vt-5mp", 40)
    run.execute_and_check("VT_5MP")


def test_cnl_vt_5hp():
    run = SimulationRun("2.021", "CNL", "RYU", "cnl-vt-5hp", 40)
    run.execute_and_check("VT_5HP")
