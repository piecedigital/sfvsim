from tests.util import SimulationRun


def test_krn_mujin_h():
    run = SimulationRun("2.021", "KRN", "RYU", "krn-mujin-h", 40)
    run.execute("STAND", 2)
    run.execute_and_check("MUJIN_H", known_error_count=337)


def test_krn_ressenha_ex():
    run = SimulationRun("2.021", "KRN", "RYU", "krn-ressenha-ex", 40)
    run.execute("STAND", 2)
    run.execute_and_check("RESSENHA_EX")

