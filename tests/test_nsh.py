from tests.util import SimulationRun


def test_nsh_moonsault_ex():
    run = SimulationRun("2.021", "NSH", "RYU", "nsh-moonsault-ex")
    run.execute("STAND", 2)
    run.execute_and_check("MOONSULT_EX")
