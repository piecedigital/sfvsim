from tests.util import SimulationRun


def test_alx_headbutt_mp():
    run = SimulationRun("2.080", "ALX", "RYU", "alx-headbutt-mp")
    run.execute_and_check("HEADBUTT_M")


def test_alx_lariat():
    run = SimulationRun("2.080", "ALX", "RYU", "alx-lariat")
    run.p1.vtrigger = 3000
    run.execute_and_check("VT_LARIAT")


def test_alx_lariat_whiff():
    run = SimulationRun("2.080", "ALX", "RYU", "alx-lariat-whiff", -10)
    run.p1.vtrigger = 3000
    run.execute_and_check("VT_LARIAT")


def test_alx_powerbomb_lp():
    run = SimulationRun("2.080", "ALX", "RYU", "alx-powerbomb-lp", 50)
    run.execute("STAND", 1)
    run.execute_and_check("POWERBOMB_L")


def test_alx_flash_lp():
    run = SimulationRun("2.080", "ALX", "RYU", "alx-flash-lp", 50)
    run.execute("STAND", 2)
    run.execute_and_check("FLASH_L")


def test_alx_slash_hp():
    run = SimulationRun("2.080", "ALX", "RYU", "alx-slash-hp")
    run.execute_and_check("SLASH_ELBOW_H")


def test_alx_knee_catch_hk():
    run = SimulationRun("2.080", "ALX", "RYU", "alx-knee-catch-hk")
    run.execute("CROUCH", 2)
    run.execute_and_check("AIRKNEE_H", script_p2="JUMP_F")
