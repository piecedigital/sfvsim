from tests.util import SimulationRun


def test_z23_vskill_reflect():
    run = SimulationRun("2.080", "Z23", "RYU", "z23-vskill-reflect")
    run.execute("STAND", 2)
    run.execute("STAND", 13, "FB_H")
    run.execute_and_check("V_SKILL")


def test_z23_vskill_absorb():
    run = SimulationRun("2.080", "Z23", "Z21", "z23-vskill-absorb", forward_p2=1)
    run.execute("STAND", 1, "STAND")
    run.execute("STAND", 18, "SEKIAGOSHOHA_L")
    run.execute_and_check("V_SKILL_H")


def test_z23_vskill_fail_absorb():
    run = SimulationRun("2.080", "Z23", "Z21", "z23-vskill-fail-absorb", forward_p2=1)
    run.execute("STAND", 1, "STAND")
    run.execute("STAND", 18, "SEKIAGOSHOHA_M")
    run.execute_and_check("V_SKILL_H")


def test_z23_vskill_fail_reflect():
    run = SimulationRun("2.080", "Z23", "Z21", "z23-vskill-fail-reflect")
    run.execute("STAND", 2)
    run.execute("STAND", 13, "GOHADO_EX")
    run.execute_and_check("V_SKILL")


