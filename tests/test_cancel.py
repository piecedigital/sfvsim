from model import PlayerState
from model.cancel import standard_move_cancels
from simulation.simulator import MoveNotAllowed
from tests.util import get_sim

BASE_CANCEL_JUMP = {'8LK', '8HK', '8HP', '8LP', '8MK', '8MP'}

sim = get_sim("2.016")


def test_common_moves():
    p1 = PlayerState("CMY", -1.5, 0)
    p2 = PlayerState("RYU", 1.5, 0, opponent=p1)
    sim.execute(p1, p2, "FORWARD", 20)
    # Can cancel forward by jump
    sim.execute(p1, p2, "JUMP_F", 20)
    try:
        sim.execute(p1, p2, "JUMP_F", 1)
        assert False
    except MoveNotAllowed as e:
        # cant double jump
        assert "height restriction" not in str(e)
    try:
        sim.execute(p1, p2, "DASH", 1)
        assert False
    except MoveNotAllowed as e:
        # cant air dash
        assert "height restriction" not in str(e)
    p1 = PlayerState("CMY", -1.5, 0)
    p2 = PlayerState("RYU", 1.5, 0, opponent=p1)
    sim.execute(p1, p2, "DASH", 5)
    try:
        sim.execute(p1, p2, "FORWARD", 1)
        assert False
    except MoveNotAllowed as e:
        # cant cancel a dash
        assert "height restriction" not in str(e)


def test_height_restriction():
    p1 = PlayerState("CMY", -1.5, 0.69)
    p2 = PlayerState("RYU", 1.5, 0, opponent=p1)
    p1.script.script = 8  # JUMP_F
    try:
        sim.execute(p1, p2, "STRIKE_L", 1)
        assert False
    except MoveNotAllowed as e:
        # cant standard strike
        assert "position restriction" in str(e)
    # Strike EX is high enough though
    try:
        sim.execute(p1, p2, "STRIKE_EX", 1)
        assert False
    except MoveNotAllowed as e:
        # cant standard strike
        assert "position restriction" in str(e)

    p1 = PlayerState("CMY", -1.5, 0.71)
    p2 = PlayerState("RYU", 1.5, 0, opponent=p1)
    p1.script.script = 8  # JUMP_F
    try:
        sim.execute(p1, p2, "STRIKE_L", 1)
        assert False
    except MoveNotAllowed as e:
        # cant standard strike
        assert "position restriction" in str(e)
    # Strike EX is high enough though
    sim.execute(p1, p2, "STRIKE_EX", 1)

    # If high enough, STRIKE_L can pass
    p1 = PlayerState("CMY", -1.5, 1.21)
    p2 = PlayerState("RYU", 1.5, 0, opponent=p1)
    p1.script.script = 8  # JUMP_F
    sim.execute(p1, p2, "STRIKE_L", 12)


def test_cammy_can_cancel():
    # Vertical and back jump can only cancel into 8 hits (pre 2.1)
    assert BASE_CANCEL_JUMP == get_cancel_names("CMY", "JUMP_V", 6)
    assert BASE_CANCEL_JUMP == get_cancel_names("CMY", "JUMP_B", 6)
    # Forward jump can STRIKE
    jump_f_cancel = BASE_CANCEL_JUMP.union({"STRIKE_L", "STRIKE_M", "STRIKE_H", "STRIKE_H", "STRIKE_EX", "V_STRIKE"})
    assert jump_f_cancel == get_cancel_names("CMY", "JUMP_F", 6)

    # 5HP can VTC_H or special cancel depending on the frame
    ground_specials = {"ARROW_L", "ARROW_M", "ARROW_H", "ARROW_EX", "SPIKE_L", "SPIKE_M", "SPIKE_H", "SPIKE_EX",
                       "V_ARROW", "V_SPIKE_L", "V_SPIKE_M", "V_SPIKE_H", "HOOLIGAN_L", "HOOLIGAN_M", "HOOLIGAN_H",
                       "HOOLIGAN_EX", "CA_L", "CA_M", "CA_H"}
    assert set() == get_cancel_names("CMY", "5HP", 4)
    assert ground_specials == get_cancel_names("CMY", "5HP", 5)
    assert {"VTC_H"} == get_cancel_names("CMY", "5HP", 7)

    # 5MP can cancel into VTC_H AND special on frame 5,6,7
    assert ground_specials.union({"VTC_M"}) == get_cancel_names("CMY", "5MP", 5)
    assert "VTC_H" in get_cancel_names("CMY", "2HP", 8)


def test_rashid_can_cancel():
    # run does have a hold condition
    assert 1000 in standard_move_cancels(sim.data.chars["RSD"], [18])


def test_akuma_can_cancel():
    assert "ZANKU_L_FD" in get_cancel_names("Z21", "JUMP_F", 11)
    assert "ZANKU_L_FU" not in get_cancel_names("Z21", "JUMP_F", 11)
    assert "ZANKU_L_FU" in get_cancel_names("Z21", "JUMP_F", 10)
    assert "82MK" in get_cancel_names("Z21", "JUMP_F", 14)
    assert "82MK" not in get_cancel_names("Z21", "JUMP_F", 13)
    assert "82MK" not in get_cancel_names("Z21", "JUMP_F", 23)


def get_cancel_names(char, move_name, frame):
    moves = sim.data.chars[char].moves
    cancels = sim.data.get_cancels(char, sim.data.chars[char].scripts[sim.data.chars[char].script_idx[move_name]], frame)
    return {moves[c].name for c in cancels}
