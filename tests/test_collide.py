from model import Box
from simulation.box_management import box_collide


def test_corner_collide():
    b = Box({"X": 0, "Y": 0, "Width": 1, "Height": 1, "TickStart": 0, "TickEnd": 1})
    b2 = Box({"X": -0.5, "Y": 0, "Width": 1, "Height": 1, "TickStart": 0, "TickEnd": 1})
    assert box_collide(b, b2)
    b2 = Box({"X": 0.5, "Y": 0, "Width": 1, "Height": 1, "TickStart": 0, "TickEnd": 1})
    assert box_collide(b, b2)
    b2 = Box({"X": -0.25, "Y": 0.5, "Width": 1, "Height": 1, "TickStart": 0, "TickEnd": 1})
    assert box_collide(b, b2)
    b2 = Box({"X": 0.5, "Y": -0.5, "Width": 1, "Height": 1, "TickStart": 0, "TickEnd": 1})
    assert box_collide(b, b2)


def test_not_collide():
    b = Box({"X": 0, "Y": 0, "Width": 1, "Height": 1, "TickStart": 0, "TickEnd": 1})
    b2 = Box({"X": -1.5, "Y": 0, "Width": 1, "Height": 1, "TickStart": 0, "TickEnd": 1})
    assert not box_collide(b, b2)
    b2 = Box({"X": 1.5, "Y": 0, "Width": 1, "Height": 1, "TickStart": 0, "TickEnd": 1})
    assert not box_collide(b, b2)
    b2 = Box({"X": -0.25, "Y": 1.2, "Width": 1, "Height": 1, "TickStart": 0, "TickEnd": 1})
    assert not box_collide(b, b2)
    b2 = Box({"X": 0.5, "Y": -1.1, "Width": 1, "Height": 1, "TickStart": 0, "TickEnd": 1})
    assert not box_collide(b, b2)


def test_over_collide():
    b = Box({"X": 0, "Y": -0.5, "Width": 1, "Height": 2, "TickStart": 0, "TickEnd": 1})
    b2 = Box({"X": -0.5, "Y": 0, "Width": 2, "Height": 1, "TickStart": 0, "TickEnd": 1})
    assert box_collide(b, b2)
